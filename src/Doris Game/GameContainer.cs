﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
namespace Doris_Game {
public class Figure {
	public Point m_pos;
	public int m_Size;
	public int id = 0;
	public int Assignation=-1; // fig.Assignation(32): container_id(8), CONTAINER_ID(8), place_id(8)
	public int place_id = -1;
	public byte colors = 0;
	double arx, ary;
	public bool Visible;
	public int Angle {
		get {
			return angle;
		} set {
			angle = (int)(value % 360);
		}
	}

	int angle;
	static int t57x;
	static int t27x;
	static int t67x;
	static int t17x;
	int wSide = 0;

	public bool Sel = false;
	bool vert = false; // true - vertikalno, false - horizontalno (kvadrat, romb)

	public bool Vertical {
		get {
			return vert;
		} set {
			if(vert != value) {
				if (vert == false) Angle = angle + 45;
				else Angle = angle - 45;
				vert = value;
			}
		}
	}

	public static bool IsInPolygon(Point[] poly, Point point) {
		// ovo je malo dugacko ali odradjuje celu stvar :)
		var coef = poly.Skip(1).Select((p, i) => (point.Y - poly[i].Y) * (p.X - poly[i].X) - (point.X - poly[i].X) * (p.Y - poly[i].Y)).ToList();
		if (coef.Any(p => p == 0))
			return true;
		for (int i = 1; i < coef.Count(); i++) {
			if (coef[i] * coef[i-1] < 0)
				return false;
		}
		return true;
	}

	// proverava sve stranice na klik misa i obelezi stranicu ako smo kliknuli
	public bool CheckPolys(Point MousePos) {
		for (int i = 0; i < 4; i++)
			if (IsInPolygon(getPolyVertex(i), MousePos)) {
				wSide = i;
				return true;
			}
		return false;
	}

	// vraca koordinate sredine stranice koja je kliknuta misem
	private Point SideGen() {
		Point pt = new Point();
		switch (wSide) {
		case 0:
			pt.X = m_pos.X;
			pt.Y = m_pos.Y + m_Size/2;
			break;
		case 1:
			pt.X = m_pos.X + m_Size / 2;
			pt.Y = m_pos.Y;
			break;
		case 2:
			pt.X = m_pos.X + m_Size;
			pt.Y = m_pos.Y + m_Size / 2;
			break;
		case 3:
			pt.X = m_pos.X + m_Size / 2;
			pt.Y = m_pos.Y + m_Size;
			break;
		}
		return pt;
	}

	private int AbsoluteAngle(int a) {
		return ((a<0)?360+a:a);
	}

	// moguci uglovi, nista izmedju
	int[] Angles = new int[] {0,45,90,135,180,225,270,315,360,45};

	// da nam heksagon ne bude slucajno okrenut na nesto izmedju :)
	public void AngleRound() {

		int cmp = angle;

		if (vert) {
			for (int i = 1; i < Angles.Length-1; i+=2) {
				if (Math.Abs(Angles[i] - cmp) <= 45) {
					angle = Angles[i];
				}
			}
		} else {
			for (int i = 0; i < Angles.Length; i+=2) {
				if (Angles[i] - cmp <= 45) {
					angle = Angles[i];
				}
			}
		}
	}

	// rotacija stranice ka strelici (misa)
	public int fn_Angle(Point p2) {
		Point p1 = SideGen();
		Point p0 = new Point(m_pos.X + m_Size / 2, m_pos.Y + m_Size / 2); // p0 centar
		double angle1 = Math.Atan2(p0.Y - p1.Y,p0.X - p1.X);
		double angle2 = Math.Atan2(p0.Y - p2.Y,p0.X - p2.X);
		return AbsoluteAngle((int)((angle2-angle1)/oetp));
	}

	// da li smo u kvadratu gde se nalazi heksagon
	public bool CheckBounds(Point MousePos) {
		if (MousePos.X > m_pos.X &&
		        MousePos.Y > m_pos.Y &&
		        MousePos.X < m_pos.X + m_Size &&
		        MousePos.Y < m_pos.Y + m_Size) {
			return true;
		} else
			return false;
	}

	const double oetp = Math.PI / 180;

	// rotira date tacke po datim izracunatim Cos(theta) -> arx, sin(theta) -> ary
	private Point gt(int x, int y) {
		Point ptRet = new Point();
		int hSize = m_Size / 2;
		ptRet.X = (int)((x - hSize) * arx - (y - hSize) * ary) + m_pos.X + hSize;
		ptRet.Y = (int)((x - hSize) * ary + (y - hSize) * arx) + m_pos.Y + hSize;
		return ptRet;
	}

	// gt verzija sa datom tackom i uglom
	private Point gtx(Point pt, float angle) {
		arx = Math.Cos(angle * oetp);
		ary = Math.Sin(angle * oetp);
		return gt(pt.X, pt.Y);
	}

	// vraca novo izracunate tacke trazenog poligona, za proveru
	/*
		  2
		1   3
		  4
	*/
	Point[] getPolyVertex(int npoly) {
		arx = Math.Cos(angle * oetp);
		ary = Math.Sin(angle * oetp);

		Point[] retval=new Point[] {};
		switch(npoly) {
		case 0: { // levo
			retval = new Point[] {
				gt(t17x, t17x),
				gt(t27x, t27x),
				gt(t27x, t57x),
				gt(t17x, t67x),
				gt(0   , t57x),
				gt(0   , t27x),
				gt(t17x, t17x)
			};
			break;
		}
		case 1: { // gore
			retval = new Point[] {
				gt(t17x, t17x),
				gt(t27x, t27x),
				gt(t57x, t27x),
				gt(t67x, t17x),
				gt(t57x,    0),
				gt(t27x,    0),
				gt(t17x, t17x)
			};
			break;
		}
		case 2: { // desno
			retval = new Point[] {
				gt(t67x,   t17x),
				gt(t57x,   t27x),
				gt(t57x,   t57x),
				gt(t67x,   t67x),
				gt(m_Size, t57x),
				gt(m_Size, t27x),
				gt(t67x,   t17x)
			};
			break;
		}
		case 3: { // dole
			retval = new Point[] {
				gt(t67x, t67x),
				gt(t57x, t57x),
				gt(t27x, t57x),
				gt(t17x, t67x),
				gt(t27x, m_Size),
				gt(t57x, m_Size),
				gt(t67x, t67x)
			};
			break;
		}
		}

		return retval;
	}

	// da li smo kliknuli na sredinu figure
	public bool BoundSquare(Point MousePos) {
		int angle = Angle;
		Point pt;
		if (angle > 0) {
			MousePos.X -= m_pos.X;
			MousePos.Y -= m_pos.Y;
			pt = gtx(MousePos, -angle);
		} else
			pt = MousePos;

		if (pt.X > m_pos.X + t27x && pt.X < m_pos.X + t57x &&
		        pt.Y > m_pos.Y + t27x && pt.Y < m_pos.Y + t57x) {
			return true;
		}
		return false;
	}

	public void DrawIt(DrawingDoris gd, bool sel) {
		gd.FillSquareAngled(gd.graphics, m_pos, m_Size, angle, colors, sel);
	}

	public void DrawIt(DrawingDoris gd) {
		gd.FillSquareAngled(gd.graphics, m_pos, m_Size, angle, colors, Sel);
		//gd.DrawSquareAngled(gd.graphics, m_pos, m_Size, angle);
	}

	public void DrawUFOFigure(DrawingDoris gd, bool isOpponent) {
		gd.FillSquareAngled(gd.graphics, m_pos, m_Size, angle, colors, Sel, isOpponent);
	}

	public void DrawItEmpty(DrawingDoris gd) {
		gd.DrawSquareAngled(gd.graphics, m_pos, m_Size, angle);
	}

	public Figure(Point pos, int size, int Id) {
		m_pos = pos;
		m_Size = size;
		angle = 0;
		t27x = m_Size * 2 / 7;
		t57x = m_Size * 5 / 7;
		t67x = m_Size * 6 / 7;
		t17x = m_Size / 7;
		id = Id;
	}
}

public class FigureContainer {
	const int CONTAINER_ID=2;
	public static int ContainerId {
		get {
			return CONTAINER_ID;
		}
	}
	int id = 0;
	public Rectangle Bounds;
	bool[] cl;
	public int m_objSize = 0;
	public Size m_size;
	public bool CheckBounds(Point fig_pos) {
		if (fig_pos.X >= Bounds.X &&
		        fig_pos.Y >= Bounds.Y &&
		        fig_pos.X <= Bounds.X + m_size.Width &&
		        fig_pos.Y <= Bounds.Y + m_size.Height)
			return true;
		else
			return false;
	}
	public void DrawIt(Graphics g) {
		g.FillRectangle(options.figureContainerColor, Bounds.X, Bounds.Y, m_size.Width, m_size.Height);
	}
	public FigureContainer(Point pt, Size size, int objSize, int ID) {
		cl = new bool[size.Height * size.Width];
		for (int i = 0; i < cl.Length; i++) cl[i] = false;
		Bounds = new Rectangle(pt.X, pt.Y, size.Width, size.Height);
		m_objSize = objSize;
		m_size = new Size(Bounds.Width * m_objSize, Bounds.Height * m_objSize);
		id = ID;
	}
	public bool Detach(ref Figure fig) {
		// fig.Assignation(32): container_id(8), CONTAINER_ID(8), place_id(8)
		if ((fig.Assignation & 0xff) >= 0 &&
		        ((fig.Assignation >> 8)&0xff) == CONTAINER_ID &&
		        (fig.Assignation>>16)==id) {
			cl[fig.Assignation & 0xff] = false;
			fig.Assignation = -1;
			return true;
		}
		return false;
	}

	public void Clear() {
		for(int i=0; i < cl.Length; i++) {
			cl[i] = false;
		}
	}

	public void ReturnFigure(Figure fig) {
		int i = fig.Assignation & 0xff;
		fig.m_pos = new Point(Bounds.X + (i % Bounds.Width) * m_objSize, Bounds.Y + ((int)(i / Bounds.Width)) * m_objSize);
	}

	public bool Attach(ref Figure fig) {
		int i=0;
		bool found = false;
		for (; i < cl.Length; i++) {
			if (!cl[i]) {
				found = true;
				break;
			}
		}
		if (found) {
			cl[i]=true;
			fig.m_pos = new Point(Bounds.X + (i % Bounds.Width) * m_objSize, Bounds.Y + ((int)(i / Bounds.Width)) * m_objSize);
			// fig.Assignation(32): container_id(8), CONTAINER_ID(8), place_id(8)
			fig.Assignation = (id<<16)|(CONTAINER_ID<<8)|i;
		}
		return found;
	}
}

public class FigurePlacer {
	const int CONTAINER_ID = 1;
	public static int ContainerId {
		get {
			return CONTAINER_ID;
		}
	}
	public int id = -1;
	public bool Vertical = false;
	public int Assignation = -1;
	public Point m_pos = new Point();
	public Point m_loc = new Point();
	public int m_size = 0;
	public void Attach(ref Figure fig) {
		Assignation = fig.id;
		fig.Vertical = Vertical;
		fig.Assignation = (CONTAINER_ID<<8)|id;
		fig.m_pos = m_pos;
	}
	public void Detach() {
		Assignation = -1;
	}
	public FigurePlacer(Point pos, bool vert, int size, int ID) {
		m_pos = pos;
		Vertical = vert;
		m_size = size;
		id = ID;
	}
	public void DrawIt(DrawingDoris gd) {
		gd.DrawSquareAngled(gd.graphics, m_pos, m_size, (Vertical) ? 45.0f : 0.0f);
	}
	public void DrawItUnknown(DrawingDoris gd) {
		if (Assignation == -1)
			gd.DrawUnknown(m_pos, m_size);
		else
			gd.DrawSquareAngled(gd.graphics, m_pos, m_size, (Vertical) ? 45.0f : 0.0f);
	}
}

class DorisGame {
	// --- hashing mapa za boju na FigCols indeks ---
	static bool hashInited = false;
	// ok, mogao sam i duzi identifikator :D
	static byte[] hash_ColorSequence_to_FigureColorIndex = new byte[256]; // bice 232 izgubljena bajta u memoriji ali to nije nista ;)
	public static void InitHash() {
		for(int i=0; i < 24; i++) {
			hash_ColorSequence_to_FigureColorIndex[ DrawingDoris.FillCol(FigCols[i]) ] = (byte)i;
		}

	}
	public static int MapColorToIndex( byte color ) {
		if (!hashInited)
			InitHash();
		return hash_ColorSequence_to_FigureColorIndex[color];
	}
	// -----------------------------------------------

	// moguce boje figura (24 figure)
	public static int[][] FigCols = new int[][] {

		new int[]{1,1,1,1},
		new int[]{1,1,1,2},
		new int[]{1,2,1,2},
		new int[]{1,2,2,2},
		new int[]{1,2,3,2},
		new int[]{1,3,1,2},
		new int[]{1,3,1,3},
		new int[]{1,3,2,3},
		new int[]{1,3,3,1},
		new int[]{1,3,3,3},

		new int[]{2,1,1,2},
		new int[]{2,1,1,3},
		new int[]{2,1,3,2},
		new int[]{2,2,2,2},
		new int[]{2,2,3,2},

		new int[]{3,1,1,1},
		new int[]{3,1,1,2},
		new int[]{3,1,2,2},
		new int[]{3,1,2,3},
		new int[]{3,2,1,3},
		new int[]{3,2,2,3},
		new int[]{3,2,3,2},
		new int[]{3,3,2,3},
		new int[]{3,3,3,3},

	};
}

public enum MapType {
	Challenge,
	Puzzle, // nije odradjeno jos ...
	UFO // nije ugradjeno, eksperimentalno
};

public struct DorisMap {
	public int id;
	public String Title;
	public byte[] m_pts;
	public MapType m_map_type;
	public Point m_resolution;
	public FigurePlacer[] fig_placers;
}
}
