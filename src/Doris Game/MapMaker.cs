﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Doris_Game {
public partial class MapMaker : Form {
	public MapMaker() {
		InitializeComponent();
	}
	const int SIZE = 100;

	Rectangle WorkField = new Rectangle(150, 100,800,800);
	Figure[] fig = new Figure[3];
	FigurePlacer[] fpl = new FigurePlacer[24];
	FigureContainer fcon;

	public DorisMap EditingMap;
	DrawingDoris gd = new DrawingDoris();
	Point rel = new Point();
	Point was = new Point();

	int zprio = -1;
	int keyState;
	public int data = 0;
	public void MapMaker_LoadMap(DorisMap map) {
		tb_game.Text = map.Title;
		EditingMap = map;
		int res = map.m_resolution.X;
		tb_res_A.Text = "" + res;
		if (res == 0) res = 6;
		for (int i = 0; i < 24; i++) {
			try {
				fpl[i] = new FigurePlacer(
				    new Point(WorkField.X + (i % res) * SIZE,
				              WorkField.Y + (i / res) * SIZE), false, SIZE, i);
				fpl[i].Assignation = 0;
				fpl[i].Vertical = (map.m_pts[i]==1);
			} catch (Exception) { }
		}
		fcon = new FigureContainer(new Point(280, 500), new Size(3, 1), SIZE, 0);
		bool ch = false;
		for (int i = 0; i < 3; i++) {
			fig[i] = new Figure(new Point(), SIZE, i);
			ch ^= true;
			fig[i].Vertical = ch;
			fcon.Attach(ref fig[i]);
		}
		this.Show();
		this.Focus();
		this.Invalidate();
		this.Update();
	}
	public void MapMaker_NewMap(DorisMap map) {
		tb_game.Text = "";
		EditingMap = map;
		int res = map.m_resolution.X;
		if(res==0)res=6;
		for (int i = 0; i < 24; i++) {
			try {
				fpl[i] = new FigurePlacer(
				    new Point(WorkField.X+(i % res) * SIZE,
				              WorkField.Y+(i / res) * SIZE), false, SIZE, i);
			} catch (Exception) { }
		}
		fcon = new FigureContainer(new Point(280, 500), new Size(3, 1), SIZE, 0);
		bool ch = false;
		for (int i = 0; i < 3; i++) {
			fig[i] = new Figure(new Point(), SIZE, i);
			ch ^= true;
			fig[i].Vertical = ch;
			fcon.Attach(ref fig[i]);
		}
		this.Show();
		this.Focus();
		this.Invalidate();
		this.Update();
	}
	private void MapMaker_Load(object sender, EventArgs e) {
		for (int i = 0; i < Controls.Count; i++) {
			Controls[i].BackColor = options.textBackColor;
			Controls[i].ForeColor = options.textForeColor;
		}
	}

	protected override void OnPaint(PaintEventArgs e) {
		Graphics g = e.Graphics;
		g.Clear(options.backColor);
		gd.graphics = g;
		for (int i = 0; i < 24; i++) {
			fpl[i].DrawItUnknown(gd);
		}
		fcon.DrawIt(g);
		for (int i = 0; i < 2; i++)
			fig[i].DrawItEmpty(gd);
		gd.DrawUnknown(fig[2].m_pos, SIZE);

	}
	private bool Check4Assign(int idFig, int idPlacer) {
		Point fig_pos = fig[idFig].m_pos;
		Point plPos = fpl[idPlacer].m_pos;
		fig_pos.X += SIZE / 2;
		fig_pos.Y += SIZE / 2;
		if (fig_pos.X > plPos.X &&
		        fig_pos.Y > plPos.Y &&
		        fig_pos.X < plPos.X + SIZE &&
		        fig_pos.Y < plPos.Y + SIZE) {
			if (idFig == 2) fpl[idPlacer].Assignation = -1;
			else {
				fpl[idPlacer].Assignation = 0;
				fpl[idPlacer].Vertical = fig[idFig].Vertical;
			}
			return true;
		} else {
			return false;
		}
	}

	private void MapMaker_MouseDown(object sender, MouseEventArgs e) {
		if(e.Button == MouseButtons.Left)
			if (keyState == 0) {
				Point mpos = this.PointToClient(Cursor.Position);
				for (int i = 0; i < 3; i++) {
					if(fig[i].BoundSquare(mpos)) {
						keyState = 1;
						zprio = i;
						rel.X = (mpos.X - fig[i].m_pos.X);
						rel.Y = (mpos.Y - fig[i].m_pos.Y);
						was = fig[i].m_pos;
						return;
					}
				}
			}
		if (e.Button == MouseButtons.Right) {
			Point mpos = this.PointToClient(Cursor.Position);
			if (zprio != -1) {
				keyState = 1;
				rel.X = SIZE/2;
				rel.Y = SIZE/2;
				was = fig[zprio].m_pos;
				MapMaker_MouseMove(sender, e);
			}
		}
	}
	private void MapMaker_MouseUp(object sender, MouseEventArgs e) {
		if (zprio < 0) return;

		Point pos = fig[zprio].m_pos;

		for (int i = 0; i < 24; i++) {
			if(Check4Assign(zprio, i)) {
				break;
			}
		}
		keyState = 0;
		fig[zprio].m_pos = was;
		Invalidate();
	}

	private void MapMaker_FormClosing(object sender, FormClosingEventArgs e) {
		this.Hide();
		Program.MSelector.OnFormShow(NEXT_ACTION.MAKE);
		e.Cancel = true;
	}

	private void MapMaker_MouseMove(object sender, MouseEventArgs e) {
		if (keyState == 1) {
			Point fpos = this.PointToClient(Cursor.Position);
			fpos.X -= rel.X;
			fpos.Y -= rel.Y;
			fig[zprio].m_pos = fpos;
			Invalidate();
		}
	}

	private void button1_Click(object sender, EventArgs e) {
		try {
			EditingMap.m_resolution.X = int.Parse(tb_res_A.Text);
		} catch (Exception) { }
		MapMaker_NewMap(EditingMap);
	}
	private byte[] ExportBytes() {
		byte[] bt = new byte[24];
		for (int i = 0; i < 24; i++)
			bt[i] = (byte)((fpl[i].Vertical) ? 1 : 0);
		return bt;
	}
	private void but_save_Click(object sender, EventArgs e) {
		EditingMap.Title = tb_game.Text;
		if (data == 1) { // nova mapa
			DorisMap[] dmtemp = new DorisMap[1];
			FileLoader fl = new FileLoader();
			if (File.Exists(options.MAP_PATH)) {
				fl.LoadFile(ref dmtemp, options.MAP_PATH, false);
				int len = dmtemp.Length;
				DorisMap[] copyMaps = dmtemp;
				dmtemp = new DorisMap[len + 1];
				copyMaps.CopyTo(dmtemp, 0);
				dmtemp[len] = EditingMap;
				dmtemp[len].m_pts=ExportBytes();
				fl.WriteToFile(dmtemp, options.MAP_PATH, false);
			} else {
				dmtemp[0] = EditingMap;
				dmtemp[0].m_pts = ExportBytes();
				fl.WriteToFile(dmtemp, options.MAP_PATH, false);
			}
			this.Hide();
			Program.MSelector.OnFormShow(NEXT_ACTION.MAKE);
		}
		if (data == 2) { // edit mapa
			DorisMap[] dmtemp = new DorisMap[1];
			FileLoader fl = new FileLoader();
			fl.LoadFile(ref dmtemp, options.MAP_PATH, false);
			EditingMap.m_pts = ExportBytes();
			dmtemp[EditingMap.id] = EditingMap;
			fl.WriteToFile(dmtemp, options.MAP_PATH, false);
		}
	}

	private void tb_res_A_TextChanged(object sender, EventArgs e) {
		try {
			EditingMap.m_resolution.X = int.Parse(tb_res_A.Text);
		} catch (Exception) { }
		String save = tb_game.Text;
		if (data == 1)
			MapMaker_NewMap(EditingMap);
		else
			MapMaker_LoadMap(EditingMap);
		tb_game.Text = save;
	}
}
}
