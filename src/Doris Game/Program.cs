﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Doris_Game {

static class Program {
	public static Singleplayer Game;
	public static MapMaker MMaker;
	public static MapSelector MSelector;
	public static StartForm SForm;
	public static UFO UFOForm;
	public static netinit NetInit;
	public static NetworkMode act = NetworkMode.None;

	public static String pl_name = "";
	public static String net_ip = "";
	public static int net_port = 0;
	public static String pl_op_name = "";

	public enum NetworkMode {
		None,
		Client,
		Server
	};


	/// <summary>
	/// The main entry point for the application.
	/// </summary>
	[STAThread]
	static void Main() {
		Application.EnableVisualStyles();
		Application.SetCompatibleTextRenderingDefault(false);

		// forme se inicijalizuju ovde, ne sme se dispose ni jedna forma, mora postojati FormClosing handler koji negira dispose forme
		Game = new Singleplayer();
		MMaker = new MapMaker();
		MSelector= new MapSelector();
		SForm = new StartForm();
		UFOForm = new UFO();
		NetInit = new netinit();

		Application.Run(SForm);
	}
}
}
