﻿namespace Doris_Game {
partial class Singleplayer {
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Windows Form Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        this.components = new System.ComponentModel.Container();
        this.loopTimer = new System.Windows.Forms.Timer(this.components);
        this.label1 = new System.Windows.Forms.Label();
        this.gameTimeTimer = new System.Windows.Forms.Timer(this.components);
        this.SuspendLayout();
        // 
        // loopTimer
        // 
        this.loopTimer.Interval = 5;
        this.loopTimer.Tick += new System.EventHandler(this.TimerLoop_Tick);
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
        this.label1.Location = new System.Drawing.Point(22, 163);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(127, 33);
        this.label1.TabIndex = 0;
        this.label1.Text = "00:00:00";
        // 
        // gameTimeTimer
        // 
        this.gameTimeTimer.Interval = 1000;
        this.gameTimeTimer.Tick += new System.EventHandler(this.GameTimeTimer_Tick);
        // 
        // Singleplayer
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.BackColor = System.Drawing.Color.DarkGray;
        this.ClientSize = new System.Drawing.Size(915, 653);
        this.Controls.Add(this.label1);
        this.DoubleBuffered = true;
        this.ForeColor = System.Drawing.Color.Transparent;
        this.Name = "Singleplayer";
        this.Text = "Singleplayer";
        this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
        this.Load += new System.EventHandler(this.Form1_Load);
        this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
        this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
        this.ResumeLayout(false);
        this.PerformLayout();

	}

	#endregion

	private System.Windows.Forms.Timer loopTimer;
	private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Timer gameTimeTimer;
}
}

