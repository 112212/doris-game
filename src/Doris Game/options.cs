﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Doris_Game {
class options {
	// gui related
	public static Color backColor = Color.FromArgb(255, 20, 20, 20);
	public static Brush textBrushForeColor = Brushes.White;
	public static Color textForeColor = Color.White;
	public static Color textBackColor = backColor;
	public static Brush selectColor = new SolidBrush( Color.FromArgb(255,50,50,50) );

	// game related
	public static Brush figureContainerColor = Brushes.Gray;
	public static Brush playFieldColor = new SolidBrush( Color.FromArgb(255, 30,30,30));

	public static bool simpleGraphic = false;

	public static bool errorChecking = true;

	// figure
	public static Brush crvena = new SolidBrush(Color.FromArgb(255, 235, 0, 0));
	public static Brush zelena = new SolidBrush(Color.FromArgb(255, 0, 180, 50));
	public static Brush plava = new SolidBrush(Color.FromArgb(255, 0,100,255));

	public static Color outlineColor = Color.White;
	public static Color errorOutlineColor = Color.Red;
	public static float outlineWidth = 1;
	public static float errorOutlineWidth = 2;

	public static int figureDefaultSize = 80;

	// figure placer
	public static bool displayMiddlePlacer = false;
	public static Brush verticalPlacerColor = Brushes.LightGray;
	public static Brush horizontalPlacerColor = Brushes.Gray;

	// neka podesavanja
	public const String MAP_PATH = "maps.dat";

}
}
