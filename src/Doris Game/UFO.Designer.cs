﻿namespace Doris_Game {
partial class UFO {
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Windows Form Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
		this.components = new System.ComponentModel.Container();
		this.timerLoop = new System.Windows.Forms.Timer(this.components);
		this.actionInterpolateTimer = new System.Windows.Forms.Timer(this.components);
		this.lb_my_score = new System.Windows.Forms.Label();
		this.net_timer = new System.Windows.Forms.Timer(this.components);
		this.lb_name = new System.Windows.Forms.Label();
		this.lb_name_op = new System.Windows.Forms.Label();
		this.label3 = new System.Windows.Forms.Label();
		this.lb_game_state = new System.Windows.Forms.Label();
		this.lb_opponent_score = new System.Windows.Forms.Label();
		this.btn_restart = new System.Windows.Forms.Button();
		this.SuspendLayout();
		//
		// timerLoop
		//
		this.timerLoop.Interval = 10;
		this.timerLoop.Tick += new System.EventHandler(this.TimerLoop_Tick);
		//
		// actionInterpolateTimer
		//
		this.actionInterpolateTimer.Interval = 20;
		this.actionInterpolateTimer.Tick += new System.EventHandler(this.TimerActionInterpolator);
		//
		// lb_my_score
		//
		this.lb_my_score.AutoSize = true;
		this.lb_my_score.Location = new System.Drawing.Point(12, 292);
		this.lb_my_score.Name = "lb_my_score";
		this.lb_my_score.Size = new System.Drawing.Size(13, 13);
		this.lb_my_score.TabIndex = 0;
		this.lb_my_score.Text = "0";
		//
		// lb_name
		//
		this.lb_name.AutoSize = true;
		this.lb_name.Location = new System.Drawing.Point(3, 49);
		this.lb_name.Name = "lb_name";
		this.lb_name.Size = new System.Drawing.Size(39, 13);
		this.lb_name.TabIndex = 1;
		this.lb_name.Text = "name1";
		//
		// lb_name_op
		//
		this.lb_name_op.AutoSize = true;
		this.lb_name_op.Location = new System.Drawing.Point(59, 49);
		this.lb_name_op.Name = "lb_name_op";
		this.lb_name_op.Size = new System.Drawing.Size(39, 13);
		this.lb_name_op.TabIndex = 2;
		this.lb_name_op.Text = "name2";
		//
		// label3
		//
		this.label3.AutoSize = true;
		this.label3.Location = new System.Drawing.Point(38, 49);
		this.label3.Name = "label3";
		this.label3.Size = new System.Drawing.Size(18, 13);
		this.label3.TabIndex = 3;
		this.label3.Text = "vs";
		//
		// lb_game_state
		//
		this.lb_game_state.AutoSize = true;
		this.lb_game_state.Location = new System.Drawing.Point(12, 365);
		this.lb_game_state.Name = "lb_game_state";
		this.lb_game_state.Size = new System.Drawing.Size(66, 13);
		this.lb_game_state.TabIndex = 4;
		this.lb_game_state.Text = "SelectFigure";
		//
		// lb_opponent_score
		//
		this.lb_opponent_score.AutoSize = true;
		this.lb_opponent_score.Location = new System.Drawing.Point(53, 292);
		this.lb_opponent_score.Name = "lb_opponent_score";
		this.lb_opponent_score.Size = new System.Drawing.Size(13, 13);
		this.lb_opponent_score.TabIndex = 6;
		this.lb_opponent_score.Text = "0";
		//
		// btn_restart
		//
		this.btn_restart.Location = new System.Drawing.Point(687, 127);
		this.btn_restart.Name = "btn_restart";
		this.btn_restart.Size = new System.Drawing.Size(75, 23);
		this.btn_restart.TabIndex = 7;
		this.btn_restart.Text = "Restart";
		this.btn_restart.UseVisualStyleBackColor = true;
		this.btn_restart.Click += new System.EventHandler(this.btn_restart_Click);
		//
		// UFO
		//
		this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
		this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		this.ClientSize = new System.Drawing.Size(790, 656);
		this.Controls.Add(this.btn_restart);
		this.Controls.Add(this.lb_opponent_score);
		this.Controls.Add(this.lb_game_state);
		this.Controls.Add(this.label3);
		this.Controls.Add(this.lb_name_op);
		this.Controls.Add(this.lb_name);
		this.Controls.Add(this.lb_my_score);
		this.DoubleBuffered = true;
		this.KeyPreview = true;
		this.Name = "UFO";
		this.Text = "UFO";
		this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UFO_FormClosing);
		this.Load += new System.EventHandler(this.UFO_Load);
		this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.UFO_MouseDown);
		this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.UFO_MouseUp);
		this.ResumeLayout(false);
		this.PerformLayout();

	}

	#endregion

	private System.Windows.Forms.Timer timerLoop;
	private System.Windows.Forms.Timer actionInterpolateTimer;
	private System.Windows.Forms.Label lb_my_score;
	private System.Windows.Forms.Timer net_timer;
	private System.Windows.Forms.Label lb_name;
	private System.Windows.Forms.Label lb_name_op;
	private System.Windows.Forms.Label label3;
	private System.Windows.Forms.Label lb_game_state;
	private System.Windows.Forms.Label lb_opponent_score;
	private System.Windows.Forms.Button btn_restart;
}
}