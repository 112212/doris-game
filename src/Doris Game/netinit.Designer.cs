﻿namespace Doris_Game {
partial class netinit {
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Windows Form Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        this.button1 = new System.Windows.Forms.Button();
        this.label1 = new System.Windows.Forms.Label();
        this.tb_ip = new System.Windows.Forms.TextBox();
        this.tb_name = new System.Windows.Forms.TextBox();
        this.label2 = new System.Windows.Forms.Label();
        this.label3 = new System.Windows.Forms.Label();
        this.button2 = new System.Windows.Forms.Button();
        this.label4 = new System.Windows.Forms.Label();
        this.tb_port = new System.Windows.Forms.TextBox();
        this.SuspendLayout();
        // 
        // button1
        // 
        this.button1.Location = new System.Drawing.Point(185, 138);
        this.button1.Name = "button1";
        this.button1.Size = new System.Drawing.Size(75, 23);
        this.button1.TabIndex = 0;
        this.button1.Text = "Join";
        this.button1.UseVisualStyleBackColor = true;
        this.button1.Click += new System.EventHandler(this.button1_Click);
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.Location = new System.Drawing.Point(87, 21);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(92, 23);
        this.label1.TabIndex = 1;
        this.label1.Text = "Doris UFO";
        // 
        // tb_ip
        // 
        this.tb_ip.Location = new System.Drawing.Point(77, 60);
        this.tb_ip.Name = "tb_ip";
        this.tb_ip.Size = new System.Drawing.Size(183, 20);
        this.tb_ip.TabIndex = 2;
        this.tb_ip.Text = "127.0.0.1";
        // 
        // tb_name
        // 
        this.tb_name.Location = new System.Drawing.Point(77, 86);
        this.tb_name.Name = "tb_name";
        this.tb_name.Size = new System.Drawing.Size(183, 20);
        this.tb_name.TabIndex = 3;
        this.tb_name.Text = "nick";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Location = new System.Drawing.Point(16, 67);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(20, 13);
        this.label2.TabIndex = 4;
        this.label2.Text = "IP:";
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Location = new System.Drawing.Point(16, 89);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(38, 13);
        this.label3.TabIndex = 5;
        this.label3.Text = "Name:";
        // 
        // button2
        // 
        this.button2.Location = new System.Drawing.Point(104, 138);
        this.button2.Name = "button2";
        this.button2.Size = new System.Drawing.Size(75, 23);
        this.button2.TabIndex = 6;
        this.button2.Text = "Host";
        this.button2.UseVisualStyleBackColor = true;
        this.button2.Click += new System.EventHandler(this.button2_Click);
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Location = new System.Drawing.Point(113, 115);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(29, 13);
        this.label4.TabIndex = 7;
        this.label4.Text = "Port:";
        // 
        // tb_port
        // 
        this.tb_port.Location = new System.Drawing.Point(158, 112);
        this.tb_port.Name = "tb_port";
        this.tb_port.Size = new System.Drawing.Size(102, 20);
        this.tb_port.TabIndex = 8;
        this.tb_port.Text = "1234";
        // 
        // netinit
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(292, 175);
        this.Controls.Add(this.tb_port);
        this.Controls.Add(this.label4);
        this.Controls.Add(this.button2);
        this.Controls.Add(this.label3);
        this.Controls.Add(this.label2);
        this.Controls.Add(this.tb_name);
        this.Controls.Add(this.tb_ip);
        this.Controls.Add(this.label1);
        this.Controls.Add(this.button1);
        this.Name = "netinit";
        this.Text = "UFO Multiplayer";
        this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.netinit_FormClosing);
        this.Load += new System.EventHandler(this.netinit_Load);
        this.ResumeLayout(false);
        this.PerformLayout();

	}

	#endregion

	private System.Windows.Forms.Button button1;
	private System.Windows.Forms.Label label1;
	private System.Windows.Forms.TextBox tb_ip;
	private System.Windows.Forms.TextBox tb_name;
	private System.Windows.Forms.Label label2;
	private System.Windows.Forms.Label label3;
	private System.Windows.Forms.Button button2;
	private System.Windows.Forms.Label label4;
	private System.Windows.Forms.TextBox tb_port;
}
}