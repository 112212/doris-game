﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
// network
using System.Net;
using System.Net.Sockets;
namespace Doris_Game {
public partial class UFO : Form {
	// ------- VARIABLES --------
	Socket sockHost;
	Socket sockClient;
	AsyncCallback asyn;
	Pen movePossibilitiesPen = new Pen( Color.Green, 1 );
	Pen attackPossibilitiesPen = new Pen( Color.Red, 1 );
	//~ int g_rep_pack = 0;
	bool net_connected = false;

	DrawingDoris gd = new DrawingDoris(true);

	// konstante
	const int ACT_MAX_PROGRESS = 50;

	bool debug = false;

	// akcije
	const int ROTATION_SPEED = 10; // 10 stepeni po otkucaju timera
	const int MOVE_SPEED = 10;

	// figure
	int figSize = 70; // 80
	// moje figure su na {0..11}
	// protivnicke figure su na {12..23}
	Figure[] g_fig = new Figure[24];

	// kontejneri
	Rectangle PlayField = new Rectangle(200, 0, 800, 800);
	Rectangle PlacerBounds = new Rectangle(0, 0, 6, 4); //rezolucija

	FigureContainer g_fcon;
	//~ FigureContainer g_fcon_o; // opponents container

	// ----------[ Game State ]-------
	//~ bool myTurn;
	int myFigsLeft;
	int opponentFigsLeft; // opponent figs
	int myScore = 0;
	int opponentScore = 0;
	enum PlayState {
		SelectFigure,
		SelectedFigure,
		MovePossibilities,
		AttackPossibilities,
		OpponentTurn,
		Turn,
		GameOver
	};
	bool gameInitialized = false;
	PlayState game_state;
	int[] placerFigureIds = new int[36];
	//~ byte[] drawing_states = new byte[36]; // mislim da je ovo nepotrebno
	ActionData g_act_status = new ActionData();
	Queue<ActionData> g_act_query = new Queue<ActionData>();
	//~ bool done = false; // ovo je nepotrebno, jer vec postoji PlayState.GameOver
	// UI - state
	MouseButtons mouse = MouseButtons.None;
	// koju figuru crtati zadnju (mora biti iznad svih ostalih)
	int zprio = 0;


	// ----------------------------

	enum ActionType {
		Finished, Turn, Move, AttackMove, Put
	};
	struct ActionData {
		public int id, gpos;
		public ActionType actionType;
		//public int lastX, lastY;
		public Axis activeAxis;
		public bool isActive;
	};
	enum Axis {
		Axis_x, Axis_y
	};
	struct PossibleMove {
		public Point point;
		public int enemyColor;
		public Common.DSIDE side;
		public PossibleMove( Point pt, int color, Common.DSIDE s ) {
			point = pt;
			enemyColor = color;
			side = s;
		}
		public PossibleMove( Point pt ) {
			point = pt;
			side = Common.DSIDE.LEFT;
			enemyColor = 0;
		}
	};

	List<PossibleMove> movePossibilities = new List<PossibleMove>();
	List<PossibleMove> attackPossibilities = new List<PossibleMove>();

	Point pback;
	// --------- END OF VARIABLES ----------------

	public UFO() {
		InitializeComponent();

		this.CreateHandle();
	}

	private void setGameState( PlayState state ) {
		game_state = state;

		//~ lb_game_state.Text = "" + state;
		if( state == PlayState.GameOver ) {
			lb_game_state.Text = (myScore > opponentScore) ? "You Win" :
			                     (myScore != opponentScore) ? "You Lose" : "Its Draw";
		} else {
			lb_game_state.Text = "" + (state == PlayState.OpponentTurn ? "Opponent Turn" : "Your Turn");
		}
		Invalidate();
	}

	// ---- HANDLERS ----
	private void UFO_Load(object sender, EventArgs e) {
		// estetika
		for (int i = 0; i < Controls.Count; i++) {
			Controls[i].BackColor = options.textBackColor;
			Controls[i].ForeColor = options.textForeColor;
		}
	}

	public void UFO_Init() {
		lb_name.Text = Program.pl_name;
		if (Program.act == Program.NetworkMode.Client) { // client
			Connect();
		} else {
			Host();
		}
	}

	protected override void OnPaint(PaintEventArgs e) {
		gd.graphics = e.Graphics;
		e.Graphics.Clear(options.backColor);

		if (gameInitialized) {
			g_fcon.DrawIt( gd.graphics );
			gd.DrawUFOTable(new Point(PlayField.X, PlayField.Y), figSize);
			for (int i = 0; i < 12; i++) {
				// crtaj sve moje figure
				if (i != zprio && g_fig[i].Visible) {
					g_fig[i].DrawIt(gd);
				}
				// crtaj protivnicke figure
				if (g_fig[i + 12].Visible) {
					g_fig[i + 12].DrawUFOFigure(gd,true);
				}
			}

			if (game_state == PlayState.MovePossibilities) {
				foreach (PossibleMove p in movePossibilities) {
					gd.DrawFigureOutline(new Point( p.point.X * figSize + PlayField.X, p.point.Y * figSize + PlayField.Y ), movePossibilitiesPen, figSize);
				}
			} else if (game_state == PlayState.AttackPossibilities || game_state == PlayState.SelectedFigure) {
				foreach (PossibleMove p in attackPossibilities) {
					gd.DrawFigureOutline(new Point( p.point.X * figSize + PlayField.X, p.point.Y * figSize + PlayField.Y ), attackPossibilitiesPen, figSize);
				}
			}
			if (zprio >= 0 && g_fig[zprio].Visible) {
				if(zprio < 12) {
					g_fig[zprio].DrawIt(gd);
				} else {
					g_fig[zprio].DrawUFOFigure(gd,true);
				}
			}
		}
	}

	private void UFO_MouseDown(object sender, MouseEventArgs e) {
		if(g_act_status.isActive) return;
		bool found = false;
		if (e.Button == MouseButtons.Left) {
			Point mpos = this.PointToClient(Cursor.Position);

			if (game_state == PlayState.SelectFigure ||
			        game_state == PlayState.AttackPossibilities ||
			        game_state == PlayState.SelectedFigure) {

				for (int i = 0; i < 12; i++) {
					if (g_fig[i].Visible && g_fig[i].CheckBounds(mpos)) {

						if(i == zprio && game_state == PlayState.AttackPossibilities) {
							// kraj poteza
							sendActionQuery();
							g_act_query.Clear();
							setGameState(PlayState.OpponentTurn);
							// blokiraj sledeci if
							found = true;
						} else if( game_state != PlayState.AttackPossibilities ) {
							mouse = MouseButtons.Left;
							pback = g_fig[i].m_pos;
							zprio = i;

							showMovePossibilities(zprio);
							setGameState( PlayState.MovePossibilities );
							// blokiraj sledeci if
							found = true;
							// pokreni drag timer
							timerLoop.Start();
						}
						break;
					}
				}
			}

			if( (game_state == PlayState.AttackPossibilities || game_state == PlayState.SelectedFigure) && !found ) {
				Point pos = getPointerFieldPos(mpos);

				PossibleMove pm = new PossibleMove(new Point(0,0), 0, Common.DSIDE.NONE);
				foreach( PossibleMove p in attackPossibilities ) {
					if( p.point == pos ) {
						found = true;
						pm = p;
						break;
					}
				}
				if( found ) {
					//MessageBox.Show("attack try");
					setGameState( PlayState.AttackPossibilities );
					// deo za poslati
					setAttackAction( zprio, pos, pm.side, pm.enemyColor );
					sendActionQuery();

					// ovaj deo je za mene
					g_act_query.Clear();
					setAttackAction( zprio, pos, pm.side, pm.enemyColor );
					executeActionQuery();
				}
			}
		} else if(e.Button == MouseButtons.Right) {
			mouse = e.Button;

			Point mpos = this.PointToClient(Cursor.Position);

			if (game_state == PlayState.SelectFigure ||
			        game_state == PlayState.SelectedFigure) {

				for (int i = 0; i < 12; i++) {
					if (g_fig[i].Visible && g_fig[i].CheckBounds(mpos)) {
						found = true;
						if( !InHands(i) ) {
							zprio = i;
							setGameState( PlayState.Turn );
							setRotAction(i, ((g_fig[i].Angle + 90) / 90) % 4);
							sendActionQuery();
							executeActionQuery();
							// pokreni drag timer
							timerLoop.Start();
						}
						break;
					}
				}
			}
		}
	}

	private void UFO_MouseUp(object sender, MouseEventArgs e) {
		mouse = MouseButtons.None;
		if( game_state == PlayState.MovePossibilities ) {
			Point pos = this.PointToClient(Cursor.Position);
			pos.X -= PlayField.X;
			pos.Y -= PlayField.Y;
			pos.X /= figSize;
			pos.Y /= figSize;

			bool found = false;
			//MessageBox.Show("checking possibilities " + pos.X + ", " + pos.Y);
			foreach( PossibleMove p in movePossibilities ) {
				if( p.point == pos ) {
					found = true;
					break;
				}
			}
			if( found ) {
				//MessageBox.Show("FOUND " + this.PointToClient(Cursor.Position));
				//~ setMoveAction( zprio, new Point( pos.X * figSize + PlayField.X, pos.Y * figSize + PlayField.Y ) );
				setMoveAction( zprio, pos );
				showAttackPossibilities( zprio );

				setFigurePos( zprio, pos );
				if( attackPossibilities.Count == 0 ) {
					// salje akcije protivniku
					sendActionQuery();
					g_act_query.Clear();
					setGameState( PlayState.OpponentTurn );
					//executeActionQuery();
				} else {
					setGameState( PlayState.AttackPossibilities );
				}
			} else {
				g_fig[zprio].m_pos = pback;
				// zprio selektovana figura
				showAttackPossibilities(zprio);
				setGameState(PlayState.SelectedFigure);
				Invalidate();
			}
		}
	}

	private void TimerLoop_Tick(object sender, EventArgs e) {
		if (g_act_status.isActive) timerLoop.Stop();
		// ako drzimo levi klik
		//lb_pos.Text = "" + this.PointToClient(Cursor.Position);
		if (mouse == MouseButtons.Left) {
			int id = zprio;
			if (id < g_fig.Length) {
				Point fpos = this.PointToClient(Cursor.Position);
				g_fig[id].m_pos = new Point( fpos.X - figSize/2, fpos.Y - figSize/2 );
				Invalidate();
			}
		}
	}

	private void UFO_FormClosing(object sender, FormClosingEventArgs e) {
		Program.UFOForm.Hide();
		CancelConnection();
		Program.NetInit.RestartForm();
		//Program.NetInit.Show();
		Program.SForm.Show();
		e.Cancel = true;
		try {
			SendToSocket(NetworkMsg.GoodBye, new byte[1]);
			sockClient.Disconnect(true);
		} catch (Exception msg) {
			if(debug)
				MessageBox.Show("void UFO_FormClosing() exception: "+ msg.Message);
		}
	}
	// ---- END OF HANDLERS ----

	private void dbg(String str) {
		MessageBox.Show("(" + Program.act + ") " + str);
	}

	// Action handler
	private void TimerActionInterpolator(object sender, EventArgs e) {
		int id = g_act_status.id;
		int gpos = g_act_status.gpos;
		int dif=0;
		Point pt;
		switch (g_act_status.actionType) {
		case ActionType.Turn:
			//~ dif = gpos*90 - g_fig[id].Angle;
			dif = Common.ClosestAngle(g_fig[id].Angle, gpos*90);
			if (Math.Abs(dif) < ROTATION_SPEED) {
				// we are done
				g_act_status.actionType = ActionType.Finished;
				g_fig[id].Angle = gpos * 90;
			} else {
				g_fig[id].Angle += Math.Sign(dif) * ROTATION_SPEED;
			}

			break;
		case ActionType.Move:
		case ActionType.AttackMove:
			// po kojoj osi ce se kretati (doris UFO dozvoljava samo 1 osu istovremeno)
			if (g_act_status.activeAxis == Axis.Axis_x) {
				dif = (gpos * figSize + PlayField.X) - g_fig[id].m_pos.X;
				if (Math.Abs(dif) < MOVE_SPEED) {
					// we're done
					g_fig[id].m_pos.X = gpos * figSize + PlayField.X;
					pt = getFigureFieldPos(g_fig[id].m_pos);
					setFigurePos(id, pt);
					g_act_status.actionType = ActionType.Finished;
				} else {
					g_fig[id].m_pos.X += Math.Sign(dif) * MOVE_SPEED;
				}
			} else { // Axis_y
				dif = (gpos * figSize+PlayField.Y) - g_fig[id].m_pos.Y;
				if (Math.Abs(dif) < MOVE_SPEED) {
					// we're done
					g_fig[id].m_pos.Y = gpos * figSize + PlayField.Y;
					pt = getFigureFieldPos(g_fig[id].m_pos);
					setFigurePos(id, pt);
					g_act_status.actionType = ActionType.Finished;
				} else
					g_fig[id].m_pos.Y += Math.Sign(dif) * MOVE_SPEED;
			}
			break;
		case ActionType.Put:
			// 		 instant postavljanje figure na mesto,
			//		 kada igrac prvi put stavlja figuru na tablu,
			//		 figura postaje vidljiva

			// da li je protivnicka figura
			gpos = g_act_status.id >= 12 ? 0 : 5;
			pt = new Point();
			Figure fig = g_fig[ g_act_status.id ];
			fig.Visible = true;
			pt.X = fig.m_pos.X;
			pt.Y = gpos * figSize + PlayField.Y;
			g_fig[ g_act_status.id ].m_pos = pt;
			pt = getFigureFieldPos( fig.m_pos );
			setFigurePos(g_act_status.id, pt);
			// sledeca radnja
			g_act_status.actionType = ActionType.Finished;
			break;
		}

		//lb_my_score.Text = "" + (dif);
		if (g_act_status.actionType == ActionType.Finished) {
			// proveri za sledece akcije
			if (g_act_query.Count > 0) {
				g_act_status = g_act_query.Dequeue();
			} else {
				g_act_status.isActive = false;
				actionInterpolateTimer.Stop();
				if( game_state == PlayState.OpponentTurn ) {
					setGameState( PlayState.SelectFigure );
				} else if( game_state == PlayState.AttackPossibilities ) {
					setGameState( PlayState.OpponentTurn );
				} else if( game_state == PlayState.Turn ) {
					setGameState( PlayState.OpponentTurn );
				}
				// proveri da li je igra zavrsena
				if( myFigsLeft == 0 || opponentFigsLeft == 0 ) {
					setGameState( PlayState.GameOver );
				}
			}
		}

		// crtaj novi frame
		Invalidate();
	}

	private void returnLastFigure() {
		if( InHands(zprio) ) {
			g_fcon.ReturnFigure(g_fig[zprio]);
		} else {
			g_fig[zprio].m_pos = pback;
		}
	}

	private void destroyFigure(int fig) {
		if(fig == -1) {
			//~ MessageBox.Show("figure not present !");
			dbg("figure not present !");
			return;
		}
		g_fig[fig].Visible = false;
		//dbg("destroying figure: " + fig);
		if( fig < 12 ) {
			opponentScore += numColorsInFigure( g_fig[fig] );
			myFigsLeft--;
			lb_opponent_score.Text = "" + opponentScore;
		} else {
			myScore += numColorsInFigure( g_fig[fig] );
			opponentFigsLeft--;
			lb_my_score.Text = "" + myScore;
		}
	}

	// postavlja figuru na polozaj u igri menjajuci reference
	// newPt = (X: {0..5}, Y: {0..5})
	private void setFigurePos(int fig, Point newPt) {
		int placer_id = newPt.X + newPt.Y*6;

		// ukoliko je napad, unisti protivnicku figuru
		if( g_act_status.actionType == ActionType.AttackMove ) {
			destroyFigure( placerFigureIds[placer_id] );
		}

		// oslobodi se prethodnih referenci
		if(g_fig[fig].place_id != -1) {
			placerFigureIds[g_fig[fig].place_id] = -1;
		} else if( fig < 12 && InHands(fig) ) {
			g_fcon.Detach(ref g_fig[fig]);
		}

		// nove reference
		g_fig[fig].place_id = placer_id;
		placerFigureIds[placer_id] = fig;

		// postavi figuru na poziciju
		g_fig[fig].m_pos = new Point( newPt.X*figSize + PlayField.X, newPt.Y*figSize + PlayField.Y );
		pback = g_fig[fig].m_pos;
	}

	// pomeri figuru sa jedne lokacije na drugu (samo po x ili po y)
	// newPt = (X: {0..5}, Y: {0..5})
	private void setMoveAction(int id, Point newPt) { // Move Action
		ActionData ad = new ActionData();
		ad.id = id;
		ad.actionType = ActionType.Move;
		ad.isActive = true;

		if( InHands(id) ) {
			// prva akcija mora biti ActionType.Put
			ActionData ad1 = new ActionData();
			ad1.id = id;
			ad1.actionType = ActionType.Put;
			ad1.isActive = true;
			ad1.gpos = 5;

			// otkaci figuru
			if(g_act_status.id < 12)
				g_fcon.Detach(ref g_fig[id]);

			g_act_query.Enqueue( ad1 );
		}

		int diffx = Math.Abs((int)(pback.X-(newPt.X*figSize+PlayField.X)));
		if (diffx > figSize/2) { // X - osa
			ad.gpos = newPt.X;
			ad.activeAxis = Axis.Axis_x;
		} else { // Y - osa
			ad.gpos = newPt.Y;
			ad.activeAxis = Axis.Axis_y;
		}

		g_act_query.Enqueue(ad);
	}

	private void setAttackAction(int id, Point newPt, Common.DSIDE side, int color) {

		rotateColorToSide(g_fig[id], color, side);

		ActionData ad = new ActionData();
		ad.id = id;
		ad.actionType = ActionType.AttackMove;
		ad.isActive = true;

		int diffx = Math.Abs((int)(pback.X-(newPt.X*figSize+PlayField.X)));
		if (diffx > figSize/2) { // X - osa
			ad.gpos = newPt.X;
			ad.activeAxis = Axis.Axis_x;
		} else { // Y - osa
			ad.gpos = newPt.Y;
			ad.activeAxis = Axis.Axis_y;
		}

		g_act_query.Enqueue(ad);

	}

	// rotiraj figuru za odredjeni ugao (angl = {0..3})
	private void setRotAction(int id, int angl) { // Rotation Action
		ActionData ad = new ActionData();
		ad.id = id;
		ad.gpos = angl;
		ad.isActive = true;

		//ad.lastX = g_fig[id].Angle;
		ad.actionType = ActionType.Turn;
		g_act_query.Enqueue(ad);
	}

	// pronadji boju, a zatim je rotiraj na 'side'
	int[] rotateRel = new int[] { 1, 3, 2 };
	void rotateColorToSide(Figure fig, int color, Common.DSIDE side) {

		// da li ima potrebe za rotacijom?
		if (Common.ColorSide(fig, side, true) == color) {
			return;
		}
		for (int i = 0; i < 3; i++) {
			if (Common.ColorSide(fig, (Common.DSIDE)(((int)side+rotateRel[i])%4), true) == color) {
				setRotAction(fig.id, fig.Angle/90 + (int)(side - (Common.DSIDE)( ((int)side+rotateRel[i])%4 )) );
				return;
			}
		}
	}

	// zaokruzi poziciju na gridu
	Point roundRectPos(Point pos) {
		if (pos.X%figSize > figSize/2) pos.X = pos.X / figSize + 1;
		else pos.X /= figSize;
		if (pos.Y%figSize > figSize/2) pos.Y = pos.Y / figSize + 1;
		else pos.Y /= figSize;
		return pos;
	}

	// proverava da li se figura nalazi na tabli (false = nije bila na tabli)
	bool InHands(int id) {
		return g_fig[id].Assignation != -1;
	}

	// samo za koordinate korisnickog unosa
	// apsolutne koordinate -> field koorinate (x: {0..6}, y: {0..6})
	private Point getPointerFieldPos(Point fieldPos) {
		Point pt_pbf = fieldPos;
		pt_pbf.X -= PlayField.X;
		pt_pbf.Y -= PlayField.Y;
		pt_pbf.X /= figSize;
		pt_pbf.Y /= figSize;
		return pt_pbf;
	}

	// zaokruzuje poziciju figure
	private Point getFigureFieldPos(Point fieldPos) {
		Point pt_pbf = fieldPos;
		pt_pbf.X = (((pt_pbf.X - PlayField.X) + figSize/2) / figSize) % 6;
		pt_pbf.Y = (((pt_pbf.Y - PlayField.Y) + figSize/2) / figSize) % 6;
		return pt_pbf;
	}

	// broj razlicitih boja u figuri
	// u odnosu na broj razlicitih broja, toliko se poteza dobija
	// i koliko razl. boja protivnicka figura ima, toliko se poena dobije
	private int numColorsInFigure(Figure fig) {
		int count = 0;
		int tmp = 0;
		int state = 0;
		for (int i = 0; i < 4; i++) {
			tmp = gd.GetCol(fig.colors, i);
			if ( (state & (1 << tmp)) == 0 ) {
				count++;
				state |= (1<<tmp);
			}
		}
		return count;
	}

	// generise mapu mogucih poteza
	private void showMovePossibilities(int fig_id) {
		int np = numColorsInFigure(g_fig[fig_id]);
		Point fp = getFigureFieldPos(g_fig[fig_id].m_pos);

		bool inHands = InHands( fig_id );
		if ( inHands ) {
			fp.Y = 5;
		}
		movePossibilities.Clear();

		if(inHands) {
			for(int i=5; i > 5-np; i--) {
				if (placerFigureIds[fp.X + i * 6] != -1) break;
				movePossibilities.Add( new PossibleMove( new Point(fp.X, i) ));
			}
		} else {
			// check up
			for (int i=fp.Y-1; i >= fp.Y-np && i >= 0; i--) {
				if (placerFigureIds[fp.X + i * 6] != -1) break;
				movePossibilities.Add( new PossibleMove( new Point(fp.X, i) ));
			}
			// check down
			for (int i = fp.Y+1; i <= fp.Y + np && i < 6; i++) {
				if (placerFigureIds[fp.X + i * 6] != -1) break;
				movePossibilities.Add( new PossibleMove( new Point(fp.X, i) ));
			}
			// check left
			for (int i = fp.X - 1; i >= 0 && i >= fp.X - np; i--) {
				if (placerFigureIds[i + fp.Y * 6] != -1)
					break;
				movePossibilities.Add( new PossibleMove( new Point(i, fp.Y) ));
			}
			// check right
			for (int i = fp.X + 1; i <= fp.X + np && i < 6; i++) {
				if (placerFigureIds[i + fp.Y * 6] != -1) break;
				movePossibilities.Add( new PossibleMove( new Point(i, fp.Y) ));
			}
		}
	}

	// generise moguce napade
	private void showAttackPossibilities(int fig_id) {

		Point fp = getFigureFieldPos( g_fig[ fig_id ].m_pos );
		Point fp2;
		Common.DSIDE side = Common.DSIDE.RIGHT;
		Common.DSIDE inv_side = Common.DSIDE.LEFT;
		int color;
		int id;
		attackPossibilities.Clear();
		// ako trenutna figura nije skroz levo (tj. levo od nje postoji polje)
		// ako postoji protivnicka figura levo, onda proveri da li je moguc napad
		if( fp.X > 0 && placerFigureIds[ fp.X - 1 + fp.Y * 6 ] >= 12 ) {
			side = Common.DSIDE.LEFT;
			fp2 = Common.GetRelativeSideCoord( fp.X, fp.Y, side, ref inv_side );
			id = placerFigureIds[ fp2.X + fp2.Y * 6 ];
			color = Common.ColorSideHorizontal( g_fig[id], inv_side );
			if( Common.ColorExist( g_fig[fig_id], color ) ) {
				attackPossibilities.Add( new PossibleMove( fp2, color, side ) );
			}
		}

		if( fp.X < 5 && placerFigureIds[ fp.X + 1 + fp.Y * 6 ] >= 12 ) {
			side = Common.DSIDE.RIGHT;
			fp2 = Common.GetRelativeSideCoord( fp.X, fp.Y, side, ref inv_side );
			id = placerFigureIds[ fp2.X + fp2.Y * 6 ];
			color = Common.ColorSideHorizontal( g_fig[id], inv_side );
			if( Common.ColorExist( g_fig[fig_id], color ) ) {
				attackPossibilities.Add( new PossibleMove( fp2, color, side ) );
			}
		}

		if( fp.Y > 0 && placerFigureIds[ fp.X + (fp.Y - 1) * 6 ] >= 12 ) {
			side = Common.DSIDE.UP;
			fp2 = Common.GetRelativeSideCoord( fp.X, fp.Y, side, ref inv_side );
			id = placerFigureIds[ fp2.X + fp2.Y * 6 ];
			color = Common.ColorSideHorizontal( g_fig[id], inv_side );
			if( Common.ColorExist( g_fig[fig_id], color ) ) {
				attackPossibilities.Add( new PossibleMove( fp2, color, side ) );
			}
		}

		if( fp.Y < 5 && placerFigureIds[ fp.X + (fp.Y + 1) * 6 ] >= 12 ) {
			side = Common.DSIDE.DOWN;
			fp2 = Common.GetRelativeSideCoord( fp.X, fp.Y, side, ref inv_side );
			id = placerFigureIds[ fp2.X + fp2.Y * 6 ];
			color = Common.ColorSideHorizontal( g_fig[id], inv_side );
			if( Common.ColorExist( g_fig[fig_id], color ) ) {
				attackPossibilities.Add( new PossibleMove( fp2, color, side ) );
			}
		}

	}

	// odradjuje akcije (prvo pozvati: sendActionQuery)
	private void executeActionQuery() { // Execution Action
		if (g_act_query.Count > 0) {
			g_act_status = g_act_query.Dequeue();
			zprio = g_act_status.id;
			actionInterpolateTimer.Start();
		}
	}

	// salje akcije odradjene protivniku
	private void sendActionQuery() {
		ActionData[] ad = new ActionData[g_act_query.Count];
		g_act_query.CopyTo(ad, 0);
		// byte[i] = ActionType
		// byte[i+1] = id
		// byte[i+2] = gpos
		// byte[i+3] = axis
		int len = g_act_query.Count;
		byte[] bytes = new byte[4*(len+1)];
		int i=0;
		//MessageBox.Show("should send");
		foreach( ActionData el in ad ) {
			//MessageBox.Show("(" + Program.act + ") sending: " + el.actionType + ", " + el.id + ", " + el.gpos + ", " + el.activeAxis);
			bytes[i] = (byte)el.actionType;
			bytes[i+1] = (byte)el.id;
			bytes[i+2] = (byte)el.gpos;
			bytes[i+3] = (byte)el.activeAxis;
			i += 4;
		}
		bytes[i] = (byte)ActionType.Finished;
		SendToSocket(NetworkMsg.Played, bytes);
	}

	// =============== Connection Handlers =========================
	// client
	public bool Connect() {
		lb_name.Text = Program.pl_name;
		try {
			sockClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			IPEndPoint ip = new IPEndPoint(IPAddress.Parse(Program.net_ip), Program.net_port);
			sockClient.BeginConnect(ip, new AsyncCallback(OnConnect), null);
			return true;
		} catch (Exception msg) {
			if(debug)
				MessageBox.Show("bool connect() exception " + msg.Message);
			return false;
		}
	}

	// server
	public void Host() {
		try {
			sockHost = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			sockHost.Bind(new IPEndPoint(IPAddress.Parse(Program.net_ip), Convert.ToInt32(Program.net_port)));
			sockHost.Listen(1);
			sockHost.BeginAccept(new AsyncCallback(OnAccept), null);
		} catch (Exception e) {
			if(debug)
				MessageBox.Show("void Host() exception: " + e.Message);
		}
	}


	public void SendToSocket(NetworkMsg code, String str) {
		byte[] bytes = Encoding.ASCII.GetBytes(str);
		byte[] sendBytes = new byte[bytes.Length+1];
		sendBytes[0] = (byte)bytes.Length;
		System.Buffer.BlockCopy(bytes, 0, sendBytes, 1, bytes.Length);
		SendToSocket(code, sendBytes);
	}

	public void SendToSocket(NetworkMsg code, byte[] bytes) {
		byte[] Send = new byte[2];
		Send[0] = (byte)(bytes.Length + 1);
		Send[1] = (byte)code;
		Send = Common.AddBytes(ref Send, ref bytes);
		try {
			sockClient.Send(Send);
		} catch (Exception msg) {
			if(debug)
				MessageBox.Show("void SendToSocket(NetworkMsg code, byte str) exception " + msg.Message);
		}
	}

	// -----------------------------------------------------

	// only for server
	private void restartGame() {
		g_fcon = new FigureContainer(new Point(200,6*figSize),new Size(6,2),figSize,0);
		for (int i = 0; i < 36; i++) {
			placerFigureIds[i] = -1;
		}
		// moj red za igru
		setGameState( PlayState.SelectFigure );

		// izaberi svih 24 figura za igru, a zatim ih posalji klijentu
		RandomList lista = new RandomList(24, DateTime.Now.Second);
		for (int i = 0; i < 24; i++) {
			lista.Push(i);
		}
		myFigsLeft = opponentFigsLeft = 12;
		myScore = opponentScore = 0;
		byte[] networkBytes = new byte[24];
		for (int i = 0; i < 12; i++) {
			g_fig[i] = new Figure(new Point(0, 0), figSize, i);
			g_fig[i].colors = DrawingDoris.FillCol(DorisGame.FigCols[lista.Pop()]);
			g_fig[i].Visible = true;
			networkBytes[i] = g_fig[i].colors;
			g_fcon.Attach(ref g_fig[i]);

			g_fig[i+12] = new Figure(new Point( 5*figSize - g_fig[i].m_pos.X + 2*PlayField.X, 0 ), figSize, 12 + i);
			g_fig[i+12].colors = DrawingDoris.FillCol(DorisGame.FigCols[lista.Pop()]);
			g_fig[i+12].Visible = false;
			g_fig[i+12].Angle = 180;
			networkBytes[i+12] = g_fig[i+12].colors;
		}
		SendToSocket(NetworkMsg.RestartGame, networkBytes);
		if(!gameInitialized) {
			gameInitialized = true;
		}
		Invalidate();
	}

	// ------- [ Cross Thread Interface ] -------
	// ovo su jadne stvari koje trazi samo C#
	enum CrossThreadCall {
		LoadForm,
		SetNick,
		ExecuteActionQuery,
		UpdateScore,
		UpdateChat,
		LeaveForm,
		SetGame,
		ChangeState,
		RestartGame
	};
	delegate void CallMainThread(CrossThreadCall param);
	private void callToMainThread(CrossThreadCall param) {
		switch(param) {
		case CrossThreadCall.LoadForm:
			Program.NetInit.Hide();
			Program.UFOForm.Show();
			break;
		case CrossThreadCall.SetNick:
			lb_name_op.Text = Program.pl_op_name;
			break;
		case CrossThreadCall.ExecuteActionQuery:
			executeActionQuery();
			break;
		case CrossThreadCall.UpdateScore:
			Invalidate();
			break;
		case CrossThreadCall.LeaveForm:
			this.Hide();
			Program.SForm.Show();
			CancelConnection();
			Program.NetInit.RestartForm();
			break;
		case CrossThreadCall.SetGame:
			Invalidate();
			break;
		case CrossThreadCall.ChangeState:
			setGameState(game_state);
			break;
		case CrossThreadCall.RestartGame:
			restartGame();
			break;
		}
	}
	private void callMainThread(CrossThreadCall param) {
		Invoke( new CallMainThread(callToMainThread), new object[] { param } );
	}
	// ------------------------------------------

	// ---- NETWORK HANDLERS ----
	public enum NetworkMsg {
		Connect = 10,
		Played,
		DesyncCheck,
		GoodBye,
		PlayerName,
		KeepAlive,
		RestartGame,
		ChatMsg
	};

	// server & client handler za primljene poruke
	private void PeerHandle(byte[] bytes) {
		NetworkMsg code = (NetworkMsg)bytes[0];
		switch (code) {
		case NetworkMsg.Connect:
			// preuzmi nick protivnika
			Program.pl_op_name = Encoding.ASCII.GetString(bytes, 2, (int)bytes[1]);
			callMainThread( CrossThreadCall.SetNick );
			break;
		case NetworkMsg.Played: // Server played move
			// byte[i] = ActionType
			// byte[i+1] = id
			// byte[i+2] = gpos
			// byte[i+3] = axis
			if(g_act_query.Count > 0) {
				if(debug)
					MessageBox.Show("ERROR: query was full");
				g_act_query.Clear();
			}
			ActionType actionType;
			actionType = (ActionType)bytes[1];
			ActionData ad = new ActionData();
			for(int i=1; actionType != ActionType.Finished; i+=4 ) {
				ad.actionType = (ActionType)actionType;
				ad.id = bytes[i+1];
				ad.gpos = bytes[i+2];
				ad.activeAxis = (Axis)bytes[i+3];
				// konverzije za nasu perspektivu
				ad.id = ad.id + 12;
				if( ad.actionType == ActionType.Move ||
				        ad.actionType == ActionType.AttackMove ||
				        ad.actionType == ActionType.Put ) {
					ad.gpos = 5 - ad.gpos;
				} else if( ad.actionType == ActionType.Turn ) {
					ad.gpos = (ad.gpos + 2) % 4;
				}
				//MessageBox.Show("(" + Program.act + ") receiving: " + ad.actionType + ", " + ad.id + ", " + ad.gpos + ", " + ad.activeAxis);
				// preuzmi novu akciju
				actionType = (ActionType)bytes[i+4];
				ad.isActive = true;
				g_act_query.Enqueue( ad );
			}
			callMainThread( CrossThreadCall.ExecuteActionQuery );
			break;
		case NetworkMsg.DesyncCheck: // Desync check
			// ovo je nepotrebno
			break;
		case NetworkMsg.GoodBye: // goodbye msg
			// client disconnected on purpose
			net_connected = false;
			try {
				sockClient.Disconnect(true);
			} catch (Exception msg) {
				if(debug)
					MessageBox.Show("void ClientHandle(int code, String str) exception " + msg.Message);
			}
			callMainThread( CrossThreadCall.LeaveForm );
			break;
		case NetworkMsg.ChatMsg:
			callMainThread( CrossThreadCall.UpdateChat );
			break;
		case NetworkMsg.RestartGame: {
			g_fcon = new FigureContainer(new Point(200,6*figSize),new Size(6,2),figSize,0);
			// samo klijent moze primiti ovu poruku
			for (int i = 0; i < 36; i++) {
				//~ drawing_states[i] = 0;
				placerFigureIds[i] = -1;
			}
			game_state = PlayState.OpponentTurn;
			if(bytes.Length < 25)
				MessageBox.Show("ERROR: not enough colors from server");
			// primiti sve izabrane figure od servera
			for(int i=0; i < 12; i++) {
				g_fig[i] = new Figure(new Point(0, 0), figSize, i);
				g_fig[i].colors = bytes[i+12+1];
				g_fig[i].Visible = true;
				g_fcon.Attach(ref g_fig[i]);

				g_fig[i+12] = new Figure(new Point( 5*figSize - g_fig[i].m_pos.X + 2*PlayField.X, 0 ), figSize, 12 + i);
				g_fig[i+12].colors = bytes[i+1];
				g_fig[i+12].Angle = 180;
				g_fig[i+12].Visible = false;
			}
			myFigsLeft = opponentFigsLeft = 12;
			myScore = opponentScore = 0;
			if(!gameInitialized) {
				gameInitialized = true;
				callMainThread( CrossThreadCall.LoadForm );
			}
			callMainThread( CrossThreadCall.ChangeState );
			callMainThread( CrossThreadCall.SetGame );
			break;
		}
		}
	}

	public void OnConnect(IAsyncResult asyn) {
		try {
			sockClient.EndConnect(asyn);
		} catch (Exception e) {
			if (debug)
				MessageBox.Show("OnConnect() exception " + e.Message);
			CancelConnection();
			return;
		}
		net_connected = true;
		SendToSocket(NetworkMsg.Connect, Program.pl_name);
		WaitForData(sockClient);
	}

	// server accept
	public void OnAccept(IAsyncResult asyn) {
		try {
			sockClient = sockHost.EndAccept(asyn);
			SendToSocket(NetworkMsg.Connect, Program.pl_name);
			WaitForData(sockClient);
		} catch (Exception msg) {
			if(debug)
				MessageBox.Show("void OnAccept(IAsyncResult asyn) exception " + msg.Message);
			Program.NetInit.RestartForm();
			return;
		}
		net_connected = true;
		callMainThread( CrossThreadCall.LoadForm );
		callMainThread( CrossThreadCall.RestartGame );
	}

	public class SocketPacket {
		public byte[] dataBuffer;
		public Socket sock;
		public SocketPacket(int velicina) {
			dataBuffer = new byte[velicina];
		}
	}

	// server & client
	public void WaitForData(Socket socket) {
		if (asyn == null) {
			asyn = new AsyncCallback(OnDataReceive);
		}
		SocketPacket pkt = new SocketPacket(0x100);
		pkt.sock = socket;
		try {
			socket.BeginReceive(pkt.dataBuffer, 0, pkt.dataBuffer.Length, SocketFlags.None, asyn, pkt);
		} catch (Exception msg) {
			net_connected = false;
			callMainThread(CrossThreadCall.LeaveForm);
			if (debug)
				MessageBox.Show("void WaitForData(Socket socket) exception " + msg.Message);
		}
	}

	byte[] packetDataBytes = new byte[0x200];
	int packetIndex = 0;
	int packetRemains = 0;
	public void OnDataReceive(IAsyncResult asyn) {
		SocketPacket pkt = (SocketPacket)asyn.AsyncState;
		int iRx = 0;
		try {
			iRx = pkt.sock.EndReceive(asyn);
		} catch (Exception msg) {
			if(debug)
				MessageBox.Show("void OnDataReceive(IAsyncResult asyn) exception " + msg.Message);
			iRx = 0;
		}
		// ako nije bio exception, nastavi sa obradom preuzetih podataka

		// packet decoupling
		int bufferIndex = 0;
		while(iRx > 0) {
			if(packetRemains == 0) {
				packetRemains = pkt.dataBuffer[bufferIndex];
				bufferIndex++;
				iRx--;
			}

			int dif = Math.Min(packetRemains, iRx);
			packetRemains -= dif;
			iRx -= dif;

			System.Buffer.BlockCopy(pkt.dataBuffer, bufferIndex, packetDataBytes, packetIndex, dif);
			bufferIndex += dif;
			packetIndex += dif;

			if(packetRemains == 0) {
				PeerHandle(packetDataBytes);
				packetIndex = 0;
			}
		}
		// ----

		if (net_connected) {
			WaitForData(pkt.sock);
		} else {
			/*
			if (Program.act == Program.ACT_CLIENT) {
				MessageBox.Show("You have been disconnected...");
			} else {
				MessageBox.Show(Program.pl_op_name + " has been disconnected...");
			}
			 */
		}
	}

	public void CancelConnection() {
		packetIndex = 0;
		packetRemains = 0;
		try {
			sockClient.Close();
		} catch (Exception e) {
			if(debug)
				MessageBox.Show("CancelConnection() exception " + e.Message);
		}
		try {
			sockHost.Close();
		} catch (Exception e) {
			if(debug)
				MessageBox.Show("CancelConnection() exception " + e.Message);
		}
		net_connected = false;
		gameInitialized = false;
	}

	private void btn_restart_Click(object sender, EventArgs e) {
		restartGame();
	}
	// ---- END OF NETWORK HANDLERS ----
}
}
