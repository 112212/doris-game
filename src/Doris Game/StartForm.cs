﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Doris_Game {
public partial class StartForm : Form {
	public StartForm() {
		InitializeComponent();
	}

	private void button2_Click(object sender, EventArgs e) {
		Program.MSelector.OnFormShow(NEXT_ACTION.MAKE);
	}

	private void button1_Click(object sender, EventArgs e) {
		Program.MSelector.OnFormShow(NEXT_ACTION.PLAY);
	}

	private void StartForm_FormClosing(object sender, FormClosingEventArgs e) {
		/*this.Hide();
		e.Cancel=true;*/
	}

	private void button3_Click(object sender, EventArgs e) {
		this.Hide();
		Program.NetInit.Show();
	}

	private void StartForm_Load(object sender, EventArgs e) {
		BackColor = options.backColor;
		for (int i = 0; i < Controls.Count; i++) {
			Controls[i].BackColor = options.textBackColor;
			Controls[i].ForeColor = options.textForeColor;
		}
	}
}
}
