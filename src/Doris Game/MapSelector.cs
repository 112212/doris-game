﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Doris_Game {
public enum NEXT_ACTION {
	PLAY=1,
	MAKE=2
};
public partial class MapSelector : Form {

	DorisMap[] maps = new DorisMap[1];
	PictureBox[] PicThumbs = new PictureBox[1];
	DrawingDoris gd = new DrawingDoris();
	int sel = -1;
	public NEXT_ACTION Next_Action;
	public bool active = false;
	public MapSelector() {
		InitializeComponent();
	}

	private void MapSelector_Load(object sender, EventArgs e) {
		for (int i = 0; i < Controls.Count; i++) {
			Controls[i].BackColor = options.textBackColor;
			Controls[i].ForeColor = options.textForeColor;
		}

	}
	public void OnFormShow(NEXT_ACTION next) {
		FlowPanel.BackColor = options.backColor;
		Next_Action = next;
		if (Next_Action == NEXT_ACTION.PLAY) {
			but_del.Hide();
			but_new.Hide();
			but_ok.Text = "Play";
		} else {
			but_new.Show();
			but_del.Show();
			but_ok.Text = "Edit";
		}
		if (File.Exists(options.MAP_PATH)) {
			FileLoader fl = new FileLoader();
			fl.LoadFile(ref maps, options.MAP_PATH, false);
			PicThumbs = new PictureBox[maps.Length];
			FlowPanel.Controls.Clear();
			for (int i = 0; i < maps.Length; i++) {
				PicThumbs[i] = new PictureBox();
				PicThumbs[i].Name = "PIC_" + i;
				PicThumbs[i].Size = new Size(200, 200);
				PicThumbs[i].Click += new EventHandler(MapSelector_Click);
				PicThumbs[i].Paint += new PaintEventHandler(MapSelector_Paint);
				FlowPanel.Controls.Add(PicThumbs[i]);
			}
			if(maps.Length>0)
				active = true;
		}
		this.Show();

	}
	protected override void OnPaint(PaintEventArgs e) {
		e.Graphics.Clear(options.backColor);
		if(active)
			for (int i = 0; i < PicThumbs.Length; i++)
				PicThumbs[i].Refresh();
	}
	void MapSelector_Paint(object sender, PaintEventArgs e) {
		int id = ReadIntFromStringInd(((PictureBox)sender).Name, 4);
		gd.graphics = e.Graphics;
		gd.DrawChallengeThumb(maps[id], new Size(180,180), id==sel);

	}
	private int ReadIntFromStringInd(String str, int ind) {
		return int.Parse(str.Substring(ind));
	}
	void MapSelector_Click(object sender, EventArgs e) {
		int id = ReadIntFromStringInd(((PictureBox)sender).Name, 4);
		sel = id;
		Invalidate();
	}

	private void but_ok_Click(object sender, EventArgs e) {
		if (active) {
			if (Next_Action == NEXT_ACTION.PLAY && sel != -1) {
				Program.SForm.Hide();
				this.Hide();
				Program.Game.LoadGame(maps[sel]);
			}
			if (Next_Action == NEXT_ACTION.MAKE && sel != -1) {
				Program.SForm.Hide();
				this.Hide();
				Program.MMaker.data = 2; // edit mapa
				Program.MMaker.MapMaker_LoadMap(maps[sel]);
			}
		}
	}

	private void btn_new_Click(object sender, EventArgs e) {
		Program.SForm.Hide();
		this.Hide();
		Program.MMaker.data = 1; // nova mapa
		Program.MMaker.MapMaker_NewMap(new DorisMap());
	}

	private void MapSelector_FormClosing(object sender, FormClosingEventArgs e) {
		this.Hide();
		Program.SForm.Show();
		e.Cancel=true;
	}

	private void btn_del_Click(object sender, EventArgs e) {
		if (active && sel != -1) {
			List<DorisMap> lst = new List<DorisMap>();
			for (int i = 0; i < maps.Length; i++)
				if (sel != i)
					lst.Add(maps[i]);
			FileLoader fl = new FileLoader();
			fl.WriteToFile(lst.ToArray(), options.MAP_PATH, false);
			OnFormShow(NEXT_ACTION.MAKE);
		}
	}
}
}
