﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace Doris_Game {
class TextureProcessor {
	// TextureProcessor.cs
	//
	// Copyright 2013 nikola <sciliquant@gmail.com>
	//
	// This program is free software; you can redistribute it and/or modify
	// it under the terms of the GNU General Public License as published by
	// the Free Software Foundation; either version 2 of the License, or
	// (at your option) any later version.
	//
	// This program is distributed in the hope that it will be useful,
	// but WITHOUT ANY WARRANTY; without even the implied warranty of
	// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	// GNU General Public License for more details.
	//
	// You should have received a copy of the GNU General Public License
	// along with this program; if not, write to the Free Software
	// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	// MA 02110-1301, USA.
	//
	//

	struct RGBA {
		public byte r;
		public byte g;
		public byte b;
		public byte a;
		public RGBA(byte red, byte green, byte blue, byte alpha) {
			r = red;
			g = green;
			b = blue;
			a = alpha;
		}
	};
	public struct UV_Vertex {
		public float u;
		public float v;
		public int x;
		public int y;
	};


	private static void CopyPictureToPictureBilinearScale( Bitmap slika1, Bitmap slika2 ) {

		int w1 = slika1.Width, h1 = slika1.Height;
		int w2 = slika2.Width, h2 = slika2.Height;

		float ratio_x = (float)(w1-1) / (float)w2;
		float ratio_y = (float)(h1-1) / (float)h2;

		int x,y,xint,yint;
		float dx, dy;
		float xi, yi;
		int R, G, B, A;

		for(y=0; y < h2; y++) {
			for(x=0; x < w2; x++) {
				// podaci za interpolaciju
				xi = ratio_x * x;
				yi = ratio_y * y;
				xint = (int)xi;
				yint = (int)yi;
				dx = xi - xint;
				dy = yi - yint;

				Color a = slika1.GetPixel(xint, yint);
				Color b = slika1.GetPixel(xint + 1, yint);
				Color c = slika1.GetPixel(xint, yint + 1);
				Color d = slika1.GetPixel(xint + 1, yint + 1);

				// bilinearna interpolacija:
				// Ca*(1-dx)*(1-dy) + Cb*dx*(1-dy) + Cc*(1-dx)*(dy) + Cd*(dx*dy)
				// sredjeno: (Ca*(1-dx)+Cb*dx)*(1-dy) + (Cc*(1-dx)+Cd*dx)*dy

				// crvena boja
				R = (int)((a.R * (1 - dx) + b.R * dx) * (1 - dy) + (c.R * (1 - dx) + d.R * dx) * dy);
				// zelena
				G = (int)((a.G * (1 - dx) + b.G * dx) * (1 - dy) + (c.G * (1 - dx) + d.G * dx) * dy);
				// plava
				B = (int)((a.B * (1 - dx) + b.B * dx) * (1 - dy) + (c.B * (1 - dx) + d.B * dx) * dy);
				// alpha
				A = (int)((a.A * (1 - dx) + b.A * dx) * (1 - dy) + (c.A * (1 - dx) + d.A * dx) * dy);

				slika2.SetPixel(x, y, Color.FromArgb(A, R, G, B));

			}
		}
	}

	private static void minmax(int p1, int p2, ref int mn, ref int mx) {
		if(p1 < p2) {
			mn = p1;
			mx = p2;
		} else {
			mx = p1;
			mn = p2;
		}

	}

	// pronaci presecnu tacku x kod prave izmedju 2 tacke i dato y

	/*      [*] vert1
	 *      /
	 *     /
	 * ---*-------- y (n)
	 *   /x
	 *  /
	 *[*] vert2
	 */
	private static int getX(UV_Vertex vert1, UV_Vertex vert2, int n) {
		int mn=0,mx=0;
		if(vert1.y == n) {
			return vert1.x;
		} else if( vert2.y == n) {
			return vert2.x;
		} else if( vert1.x == vert2.x ) {
			minmax(vert1.y, vert2.y, ref mn, ref mx);
			if( n >= mn && n <= mx )
				return vert1.x;
		} else {
			float k = (float)(vert1.y - vert2.y) / (float)(vert1.x - vert2.x);
			float fx = (k*vert1.x-vert1.y+n)/k;
			minmax(vert1.x, vert2.x, ref  mn, ref mx);
			if(fx >= mn && fx <= mx)
				return (int)fx;
			else
				return -1;
		}
		return -1;
	}

	// ovo je ista ta funkcija samo nisu UV_Vertexi u pitanju nego samo tacke C# (Point tipa)
	// sluzi za [ TexturePolygonCopyParallel ] funkciju :)
	private static int getX(Point vert1, Point vert2, int n) {
		int mn = 0, mx = 0;
		if (vert1.Y == n) {
			return vert1.X;
		} else if (vert2.Y == n) {
			return vert2.X;
		} else if (vert1.X == vert2.X) {
			minmax(vert1.Y, vert2.Y, ref mn, ref mx);
			if (n >= mn && n <= mx)
				return vert1.X;
		} else {
			float k = (float)(vert1.Y - vert2.Y) / (float)(vert1.X - vert2.X);
			float fx = (k * vert1.X - vert1.Y + n) / k;
			minmax(vert1.X, vert2.X, ref  mn, ref mx);
			if (fx >= mn && fx <= mx)
				return (int)fx;
			else
				return -1;
		}
		return -1;
	}

	/*      [*] vert1
	 *      /
	 *     /
	 * ---*-------- y (n)
	 *   /x
	 *  /
	 *[*] vert2
	 */

	// ovo je floating point verzija
	private static bool getXf(UV_Vertex vert1, UV_Vertex vert2, int n, ref float res) {
		int mn=0,mx=0;
		if(vert1.y == n) {
			res = vert1.x;
			return true;
		} else if( vert2.y == n) {
			res = vert2.x;
			return true;
		} else if( vert1.x == vert2.x ) {
			minmax(vert1.y, vert2.y, ref mn, ref mx);
			if( n >= mn && n <= mx ) {
				res = vert1.x;
				return true;
			}
		} else {
			float k = (float)(vert1.y - vert2.y) / (float)(vert1.x - vert2.x);
			float fx = (k*vert1.x-vert1.y+n)/k;
			minmax(vert1.x, vert2.x, ref mn, ref mx);
			if(fx >= mn && fx <= mx) {
				res = fx;
				return true;
			}
		}
		return false;
	}

	public static void TextureMap(Bitmap slika1, UV_Vertex[] vertices, ref Bitmap slika2) {
		if(vertices.Length < 3) return;

		//
		int w1 = slika1.Width, h1 = slika1.Height;
		int w2 = slika2.Width, h2 = slika2.Height;

		int i,j,x,y,x1,x2;
		float x1f=0, x2f=0;
		float dx,dy;
		float xi, yi;
		int next_y=-1;
		int xint, yint;
		int R, G, B, A;
		UV_Vertex vtx1,vtx2;
		UV_Vertex[] pts = new UV_Vertex[4];
		int num;
		bool found;
		//

		for(y=0; y < h2; y++) {

			if(y > next_y) {
				found = false;
				num = 0;
				x1=0;
				// pronaci x
				for(i=0; i < vertices.Length-1; i++) {
					x=getX(vertices[i], vertices[i+1], y);
					if (x == x1) continue;
					x1 = x;
					if(x!=-1) {
						pts[num++] = vertices[i];
						pts[num++] = vertices[i+1];
					}
					if(num >= 4) {
						found = true;
						break;
					}
				}

				// provera da li smo zavrsili
				if(!found)
					continue;
				else
					next_y = Math.Min(Math.Max(pts[0].y, pts[2].y), Math.Max(pts[1].y, pts[3].y));
			}

			UV_Vertex a1 = pts[0];
			UV_Vertex b1 = pts[1];
			UV_Vertex c1 = pts[2];
			UV_Vertex d1 = pts[3];
			// ovde smo vec nasli 4 vertexa
			// pronaci scanlinove

			// a b
			// c d

			getXf( a1, b1, y, ref x1f );
			getXf( c1, d1, y, ref x2f );

			x1 = (int)x1f;
			x2 = (int)x2f;

			minmax(x1, x2, ref x1, ref x2);

			// izracunaj u,v za vtx1,vtx2
			dy = getDistanceRect(b1.x, b1.y, a1.x, a1.y);
			dx = (dy==0) ? 0 : getDistanceRect(x1f, y, a1.x, a1.y) / dy;

			// u,v za vtx1
			vtx1.u = a1.u*(1-dx)+b1.u*dx;
			vtx1.v = a1.v*(1-dx)+b1.v*dx;

			dy = getDistanceRect(d1.x, d1.y, c1.x, c1.y);
			dx = (dy == 0) ? 0 : getDistanceRect(x2f, y, c1.x, c1.y) / dy;

			// u,v za vtx2
			vtx2.u = c1.u*(1-dx)+d1.u*dx;
			vtx2.v = c1.v*(1-dx)+d1.v*dx;

			for(i=x1,j=x2; i < j; i++) {
				dx = (float)(i-x1f)/(float)(x2f-x1f);

				// interpolacije pixela (px -> px2) tj. trazimo odgovarajuci pixel u izvornoj slici da izvrsimo bilinearnu interpolaciju
				xi = (vtx1.u*(1-dx)+vtx2.u*dx)*(float)w1;
				yi = (vtx1.v*(1-dx)+vtx2.v*dx)*(float)h1;

				xint = (int)xi;
				yint = (int)yi;

				dx = xi - xint;
				dy = yi - yint;

				if (xint + 1 >= w1) {
					xint = w1 - 2;
				}
				if (yint + 1 >= h1) {
					yint = h1 - 2;
				}

				Color a = slika1.GetPixel(xint, yint);
				Color b = slika1.GetPixel(xint + 1, yint);
				Color c = slika1.GetPixel(xint, yint + 1);
				Color d = slika1.GetPixel(xint + 1, yint + 1);

				// bilinearna interpolacija:
				// Ca*(1-dx)*(1-dy) + Cb*dx*(1-dy) + Cc*(1-dx)*(dy) + Cd*(dx*dy)
				// sredjeno: (Ca*(1-dx)+Cb*dx)*(1-dy) + (Cc*(1-dx)+Cd*dx)*dy

				// crvena boja
				R = (int)((a.R * (1 - dx) + b.R * dx) * (1 - dy) + (c.R * (1 - dx) + d.R * dx) * dy);
				// zelena
				G = (int)((a.G * (1 - dx) + b.G * dx) * (1 - dy) + (c.G * (1 - dx) + d.G * dx) * dy);
				// plava
				B = (int)((a.B * (1 - dx) + b.B * dx) * (1 - dy) + (c.B * (1 - dx) + d.B * dx) * dy);
				// alpha
				A = (int)((a.A * (1 - dx) + b.A * dx) * (1 - dy) + (c.A * (1 - dx) + d.A * dx) * dy);

				slika2.SetPixel(i, y, Color.FromArgb(A, R, G, B));
			}
		}
	}

	public static void TexturePolygonCopyParallel(Bitmap slika1, Point[] vertices, ref Bitmap slika2) {
		if (vertices.Length < 3) return;
		//
		int w1 = slika1.Width, h1 = slika1.Height;
		int w2 = slika2.Width, h2 = slika2.Height;
		int i, j, x, y, x1, x2;
		int next_y = -1;
		Point[] pts = new Point[4];
		int num;
		bool found;
		//

		for (y = 0; y < h2; y++) {

			if (y > next_y) {
				found = false;
				num = 0;
				x1 = 0;
				// pronaci 2 prave kroz koje prolazi horizontalna y prava
				for (i = 0; i < vertices.Length - 1; i++) {
					x = getX(vertices[i], vertices[i + 1], y);
					if ( x == x1 ) continue;
					x1 = x;
					if (x != -1) {
						pts[num++] = vertices[i];
						pts[num++] = vertices[i + 1];
					}
					if (num >= 4) {
						found = true;
						break;
					}
				}
				// provera da li smo zavrsili
				if (!found)
					continue;
				next_y = Math.Min(Math.Max(pts[0].Y, pts[2].Y), Math.Max(pts[1].Y, pts[3].Y));
			}

			// pronaci scanlinove
			x1 = getX(pts[0], pts[1], y);
			x2 = getX(pts[2], pts[3], y);

			minmax(x1, x2, ref x1, ref x2);

			for (i = x1, j = x2; i < j; i++) {
				slika2.SetPixel(i, y, slika1.GetPixel(i, y));
			}
		}
	}

	static double getDistance(int x1, int y1, int x2, int y2) {
		double xx = (x1-x2);
		double yy = (y1-y2);
		return Math.Sqrt( xx*xx + yy*yy );
	}

	static float getDistanceRect(float x1, float y1, float x2, float y2) {
		return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
	}
}
}
