﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Doris_Game {
public partial class puzzle : Form {
	public puzzle() {
		InitializeComponent();
	}

	const int FIGURES_COUNT = 24;

	List<FigurePlacer> list_canPlaceFigures = new List<FigurePlacer>();
	List<FigurePlacer> list_takenFigurePlacers = new List<FigurePlacer>();

	Figure[] g_fig = new Figure[FIGURES_COUNT];


	private void puzzle_Load(object sender, EventArgs e) {
		// theme
		for (int i = 0; i < Controls.Count; i++) {
			Controls[i].BackColor = options.textBackColor;
			Controls[i].ForeColor = options.textForeColor;
		}
	}

	private void LoadGame() {

	}

	private void SaveGame() {

	}
}
}
