﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
namespace Doris_Game {
public class DrawingDoris {
	public Graphics graphics = null;
	static Pen OutlineColor = new Pen(options.outlineColor, options.outlineWidth);
	static Pen ErrorOutlineColor = new Pen(options.errorOutlineColor, options.errorOutlineWidth);
	static Pen SelGray = new Pen(Brushes.Green);
	static Brush BlackB = Brushes.Black;
	const double oetp = Math.PI/180;

	private int m_size;
	private int m_last_size=0;
	private Point m_pos;
	static Brush[] Cols = new Brush[] { Brushes.Black, options.crvena, options.zelena, options.plava };

	// predefinisane slike za brzo crtanje
	static Bitmap[] m_picture_templates = new Bitmap[3]; // 3 vrste boje = 3 slike
	static TextureBrush[] TxBrush = new TextureBrush[3];
	static TextureBrush[] m_picture_preload = new TextureBrush[24];
	static TextureBrush[] m_picture_preload_opponents = new TextureBrush[24];

	// predefinisani poligoni za brzo crtanje
	public static Point[][] m_horizontal_point_matrix;// = new Point[6][];
	public static PointF[][] m_horizontal_point_matrix_f;// = new PointF[6][];

	float t17xf;
	float t27xf;
	float t57xf;
	float t67xf;

	int t17x;
	int t27x;
	int t57x;
	int t67x;

	float scale;

	public const int STAT_SEL = 0x20;
	public DrawingDoris(bool isUFO = false) {
		InitNewSize(options.figureDefaultSize, isUFO);
	}

	// TODO: ove stvari prebaciti u Common klasu
	// 1 - crvena, 2 - zelena, 3 - plava } = [2 bit]*4 = 8 bita, 1 bajt
	// pakovanje boje u bajt
	public static byte FillCol(int col1, int col2, int col3, int col4) {
		return (byte)((col1 << 6) | (col2 << 4) | (col3 << 2) | (col4));
	}

	// vraca upakovane boje date nizom boja
	public static byte FillCol(int[] col) {
		return (byte)((col[0] << 6) | (col[1] << 4) | (col[2] << 2) | (col[3]));
	}

	// otpakuje odredjenu boju po indeksu
	public int GetCol(byte colors, int ind) {
		// ind mora biti [ 0 .. 3 ]
		return (colors >> ((3 - ind) * 2)) & 0x3;
	}

	// posto je u pitanju crtanje, ovde radije koristim transformacione matrice za crtanje,
	// dok kod GameContainer koristim rucne rotacije, radi interaktivnosti
	private PointF gt(float x, float y) {
		return new PointF(x, y);
	}

	public void DrawUnknown(Point pos, int size) {
		graphics.ResetTransform();
		graphics.DrawRectangle(OutlineColor, pos.X, pos.Y, size, size);
		graphics.DrawString("?", new Font("Arial", size, FontStyle.Regular, GraphicsUnit.Pixel),
		                    options.textBrushForeColor, (PointF)pos);
	}

	// inicijalizacija lokalnih heksagon tacaka
	void InitTValues( int size ) {
		t17xf = (float)size / 7.0f;
		t27xf = (float)size * 2.0f / 7.0f;
		t57xf = (float)size * 5.0f / 7.0f;
		t67xf = (float)size * 6.0f / 7.0f;

		t17x = (int)t17xf;
		t27x = (int)t27xf;
		t57x = (int)t57xf;
		t67x = (int)t67xf;
	}

	private void InitNewSize(int size, bool isUFO = false) {
		size = options.figureDefaultSize;
		InitTValues(size);
		// ugao = 0
		//arx = 1.0;
		//ary = 0.0;
		m_size = size / 2;

		m_horizontal_point_matrix_f = new PointF[][] {
			// levo (0)
			new PointF[]{
				gt(t17xf, t17xf),
				gt(t27xf, t27xf),
				gt(t27xf, t57xf),
				gt(t17xf, t67xf),
				gt(0, t57xf),
				gt(0, t27xf),
				gt(t17xf, t17xf)
			},
			// gore (1)
			new PointF[]{
				gt(t17xf, t17xf),
				gt(t27xf, t27xf),
				gt(t57xf, t27xf),
				gt(t67xf, t17xf),
				gt(t57xf, 0),
				gt(t27xf, 0),
				gt(t17xf, t17xf)
			},
			// desno (2)
			new PointF[]{
				gt(t67xf, t17xf),
				gt(t57xf, t27xf),
				gt(t57xf, t57xf),
				gt(t67xf, t67xf),
				gt(size, t57xf),
				gt(size, t27xf),
				gt(t67xf, t17xf)
			},
			// dole (3)
			new PointF[]{
				gt(t67xf, t67xf),
				gt(t57xf, t57xf),
				gt(t27xf, t57xf),
				gt(t17xf, t67xf),
				gt(t27xf, size),
				gt(t57xf, size),
				gt(t67xf, t67xf)
			},
			// sredina (4)
			new PointF[]{
				gt(t27xf, t27xf),
				gt(t57xf, t27xf),
				gt(t57xf, t57xf),
				gt(t27xf, t57xf),
				gt(t27xf, t27xf)
			},
			// outline (5)
			new PointF[] {
				gt(t27xf, 0),
				gt(t57xf, 0),
				gt(size, t27xf),
				gt(size, t57xf),
				gt(t57xf, size),
				gt(t27xf, size),
				gt(0,t57xf),
				gt(0,t27xf),
				gt(t27xf, 0)
			}
		};

		int i, j;
		m_horizontal_point_matrix = new Point[6][];
		for (i = 0; i < 6; i++) {
			m_horizontal_point_matrix[i] = new Point[m_horizontal_point_matrix_f[i].Length];
			for (j = 0; j < m_horizontal_point_matrix_f[i].Length; j++) {
				m_horizontal_point_matrix[i][j] = new Point((int)m_horizontal_point_matrix_f[i][j].X,
				        (int)m_horizontal_point_matrix_f[i][j].Y);
			}

		}


		PrepareTexture(size, isUFO);
		//m_last_size = -1;
	}

	void PrepareTexture( int size, bool loadUFO = false ) {
		int i, j, k, c;
		if (options.simpleGraphic) return;
		// texture mapping
		Bitmap[] slika1_bitmaps = new Bitmap[] { Properties.Resources.red, Properties.Resources.green,
		        Properties.Resources.blue, Properties.Resources.blackmid, Properties.Resources.opponent
		                                       };
		int[] vert_pts = new int[] { 0,10,10,0,40,0,50,10,39,21,11,21,0,10, // red (7)
		                             0,10,10,0,40,0,50,10,40,20,9,20,0,10,  // green (7)
		                             0,10,10,0,39,0,49,10,39,20,10,20,0,10, // blue (7)
		                             0,0,1,0,1,1,0,1,0,0
		                           }; // midblack (5)
		// midblack ( cela slika )

		TextureProcessor.UV_Vertex[][] col_vertices = new TextureProcessor.UV_Vertex[4][];
		for (i = 0; i < 3; i++) {
			col_vertices[i] = new TextureProcessor.UV_Vertex[7];
			for (j = 0; j < 7; j++) {
				col_vertices[i][j].u = (float)vert_pts[i * 14 + j * 2] / (float)slika1_bitmaps[i].Width;
				col_vertices[i][j].v = (float)vert_pts[i * 14 + j * 2 + 1] / (float)slika1_bitmaps[i].Height;
			}
		}
		col_vertices[3] = new TextureProcessor.UV_Vertex[5];
		for (i = 0; i < 5; i++) {
			col_vertices[3][i].u = (float)vert_pts[3 * 14 + i * 2];
			col_vertices[3][i].v = (float)vert_pts[3 * 14 + i * 2 + 1];
			col_vertices[3][i].x = (int)m_horizontal_point_matrix[4][i].X;
			col_vertices[3][i].y = (int)m_horizontal_point_matrix[4][i].Y;
		}

		for (i = 0; i < 3; i++) {
			m_picture_templates[i] = new Bitmap(size, size);

			// set vert x,y and go

			for (c = 0; c < 4; c++) { // C++ rules :D
				for (k = 0; k < 7; k++) {
					// horizontalno
					col_vertices[i][k].x = (int)m_horizontal_point_matrix[c][k].X;
					col_vertices[i][k].y = (int)m_horizontal_point_matrix[c][k].Y;
				}

				TextureProcessor.TextureMap(slika1_bitmaps[i], col_vertices[i], ref m_picture_templates[i]);
			}
			TextureProcessor.TextureMap(slika1_bitmaps[3], col_vertices[3], ref m_picture_templates[i]);
		}
		for (i = 0; i < 3; i++)
			TxBrush[i] = new TextureBrush(m_picture_templates[i]);

		// preload 24 figure
		// ovako rendering ide mnogo brze
		Bitmap nova_slika;
		for (i = 0; i < 24; i++) {
			nova_slika = new Bitmap(size, size);
			int[] arr = DorisGame.FigCols[i];
			for (j = 0; j < 4; j++) {
				TextureProcessor.TexturePolygonCopyParallel(m_picture_templates[arr[j] - 1], m_horizontal_point_matrix[j], ref nova_slika);
			}
			TextureProcessor.TextureMap(slika1_bitmaps[3], col_vertices[3], ref nova_slika);
			m_picture_preload[i] = new TextureBrush(nova_slika);
			if(loadUFO) {
				TextureProcessor.TextureMap(slika1_bitmaps[4], col_vertices[3], ref nova_slika);
				m_picture_preload_opponents[i] = new TextureBrush(nova_slika);
			}

		}

	}

	TextureBrush GetTBrushBitmap(int color) {
		return TxBrush[color - 1];
	}

	TextureBrush GetTBrushFigure(byte color_sequence, bool isOpponent = false) {
		int ind = DorisGame.MapColorToIndex(color_sequence);

		/*
		m_picture_preload[ind].ResetTransform();
		m_picture_preload[ind].ScaleTransform(scale, scale);
		 */
		if(isOpponent) {
			return m_picture_preload_opponents[ind];
		} else {
			return m_picture_preload[ind];
		}
	}

	// UFO
	public void DrawUFOTable(Point pos, int size) {
		for(int x = 0; x < 6; x++) {
			for(int y = 0; y < 6; y++) {
				DrawFigureOutline( new Point(pos.X + size * x, pos.Y + size * y), OutlineColor, size );
			}
		}
	}

	public void DrawFigureOutline(Point pos, Pen outlineColor, int size) {
		if(size != m_last_size) {
			m_last_size = size;
			scale = (float)size / (float)options.figureDefaultSize;
			m_size = m_last_size / 2;
		}

		graphics.ResetTransform();
		graphics.TranslateTransform(pos.X, pos.Y);
		graphics.ScaleTransform(scale, scale);

		//~ graphics.DrawPolygon(outlineColor, OutlinePoints);
		graphics.DrawPolygon(outlineColor, m_horizontal_point_matrix_f[5]);
		// TODO: da li treba graphics.ResetTransform ovde?
	}

	public void DrawSquareAngled(Graphics g, Point pos, int size, float angle) {
		if(size != m_last_size) {
			m_last_size = size;
			scale = (float)size / (float)options.figureDefaultSize;
			m_size = m_last_size / 2;
		}

		g.ResetTransform();

		g.TranslateTransform(pos.X + m_size, pos.Y + m_size);
		g.RotateTransform( angle );
		g.TranslateTransform(-m_size, -m_size);
		g.ScaleTransform(scale, scale);

		for (int i = 0; i < 4; i++) {
			g.DrawPolygon(OutlineColor, m_horizontal_point_matrix_f[i]);
		}

		if (options.displayMiddlePlacer) {
			// square mid
			if (angle % 90 == 0)
				g.FillPolygon(options.horizontalPlacerColor, m_horizontal_point_matrix_f[4]);
			else
				g.FillPolygon(options.verticalPlacerColor, m_horizontal_point_matrix_f[4]);
		}
		g.DrawPolygon(OutlineColor, m_horizontal_point_matrix_f[4]);
	}

	public void SetFigureSize( int newSize )  {
		if(m_last_size != newSize) {
			//InitTValues(newSize);
			InitNewSize(newSize);
		}
	}

	// za greske
	public void DrawOutlinePolygons(Graphics g, Point pos, byte sides) {
		g.ResetTransform();
		g.TranslateTransform(pos.X, pos.Y);
		g.ScaleTransform(scale, scale);
		for (int i = 0; i < 4; i++)
			if ((sides & (1 << i)) != 0)
				g.DrawPolygon(ErrorOutlineColor, m_horizontal_point_matrix[i]);
	}

	private void DrawThumbVertHoriz(Rectangle bounds, bool vert) {
		DrawSquareAngled(graphics, bounds.Location, bounds.Width, ((vert) ? 45.0f : 0.0f));
		if(graphics!=null)
			graphics.ResetTransform();
	}

	public void DrawChallengeThumb(DorisMap map, Size sz, bool sel) {
		if (map.m_pts == null) return;
		int len = map.m_pts.Length;
		if (sel)
			graphics.FillRectangle(options.selectColor, new Rectangle(0, 0, sz.Width, sz.Height));
		Size bounds = sz;
		bounds.Width /= 6;
		bounds.Height /= 6;
		graphics.DrawRectangle(OutlineColor, 0, 0, sz.Width, sz.Height);
		Point res = map.m_resolution;
		if (res.X == 0) res.X = 6;
		for (int i = 0; i < len; i++)
			DrawThumbVertHoriz(
			    new Rectangle((i % res.X) * bounds.Width, (i / res.X) * bounds.Height, bounds.Width, bounds.Height),
			    map.m_pts[i] == 1);

		graphics.DrawString(map.Title.Substring(0, Math.Min(map.Title.Length, sz.Width/9)),
		                    new Font("Arial", 15, FontStyle.Bold, GraphicsUnit.Pixel), options.textBrushForeColor,
		                    new PointF(5, sz.Height*5/6));
	}

	//~ float[] sqr = new float[] { 90.0f, 0.0f, 90.0f, 0.0f };
	public void FillSquareAngled(Graphics g, Point pos, int size, float angle, byte col, bool sel, bool isOpponent = false) {
		if (size != m_last_size) {
			m_last_size = size;
			scale = (float)size / (float)options.figureDefaultSize;
		}
		m_size = m_last_size / 2;
		g.ResetTransform();
		g.TranslateTransform(pos.X+m_size, pos.Y+m_size);
		if (options.simpleGraphic) {
			g.ScaleTransform(scale, scale);
			g.RotateTransform(angle);
			g.TranslateTransform(-m_size, -m_size);
			int col_i;
			for (int i = 0; i < 4; i++) {
				col_i = (col >> ((3 - i) * 2)) & 3;

				g.FillPolygon(Cols[col_i], m_horizontal_point_matrix[i]);
				g.DrawPolygon(OutlineColor, m_horizontal_point_matrix[i]);
			}

			// square mid
			g.FillPolygon(Brushes.LightGray, m_horizontal_point_matrix[4]);
			//g.DrawPolygon(Black, m_horizontal_point_matrix[4]);
		} else {
			g.RotateTransform(angle);
			g.TranslateTransform(-m_size, -m_size);

			// koristimo preprocesovane kombinacije boja
			TextureBrush tb = GetTBrushFigure(col, isOpponent);
			tb.ResetTransform();
			tb.ScaleTransform(scale, scale);
			g.FillRectangle(tb, new Rectangle(0, 0, m_last_size, m_last_size));
		}

		// selekcija
		if(sel) {
			g.DrawPolygon(new Pen(options.selectColor, 2), m_horizontal_point_matrix[5]);
		}
	}
}
}
