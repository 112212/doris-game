﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Doris_Game {
public partial class netinit : Form {
	public netinit() {
		InitializeComponent();
	}

	private void button1_Click(object sender, EventArgs e) { // Join button
		Program.pl_name = tb_name.Text;
		Program.act = Program.NetworkMode.Client;
		Program.net_ip = tb_ip.Text;
		Program.net_port = Convert.ToInt32(tb_port.Text);
		Program.UFOForm.UFO_Init();
	}

	private void button2_Click(object sender, EventArgs e) { // Host button
		if(Program.act == Program.NetworkMode.Server) {
			Program.UFOForm.CancelConnection();
			RestartForm();
			return;
		}
		Program.pl_name = tb_name.Text;
		Program.act =  Program.NetworkMode.Server;
		Program.net_ip = "0.0.0.0";
		Program.net_port = Convert.ToInt32(tb_port.Text);
		button2.Text = "stop";
		button1.Enabled = false;
		Program.UFOForm.UFO_Init();
	}

	private void netinit_Load(object sender, EventArgs e) {
		BackColor = options.backColor;
		for (int i = 0; i < Controls.Count; i++) {
			Controls[i].BackColor = options.textBackColor;
			Controls[i].ForeColor = options.textForeColor;
		}
	}

	public void RestartForm() {
		button1.Enabled = true;
		button2.Text = "Host";
		Program.act = Program.NetworkMode.None;
	}

	private void netinit_FormClosing(object sender, FormClosingEventArgs e) {
		e.Cancel = true;
		this.Hide();
		Program.SForm.Show();
	}
}
}
