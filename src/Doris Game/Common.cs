﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
namespace Doris_Game {
class Common {
	class Word {
		public byte upper = 0;
		public byte lower = 0;
	}

	public static String FormatTime(int timer) {
		int hr = timer / 3600;
		int min = timer / 60 % 60;
		int sec = timer % 60;
		return ((hr < 10) ? "0" : "") + hr + ":" + ((min < 10) ? "0" : "") + min + ":" + ((sec < 10) ? "0" : "") + sec;
	}

	// TODO: izbaci bit operacije iz igre !
	public static int HIWORD(int wrd) {
		return (wrd >> 8) & 0xff;
	}
	public static int LOWORD(int wrd) {
		return wrd & 0xff;
	}
	public static int HIDWORD(int dwrd) {
		return (dwrd >> 16) & 0xffff;
	}
	public static int LODWORD(int dwrd) {
		return dwrd & 0xffff;
	}

	public void DumpStr(String dmp) {
		String hhhg = "";
		for (int i = 0; i < dmp.Length; i++)
			hhhg += String.Format("{0:X}", (byte)dmp[i])+ " ";
		MessageBox.Show("Dump: "+hhhg);
	}

	// TODO: izbaci bit operacije iz igre !
	public static int bitsToSignedInt(int rawBits, int signBit) {

		if ((rawBits & signBit) > 0) {
			rawBits = ((~rawBits) + 1) | signBit;
		}

		return rawBits;
	}

	// TODO: izbaci bit operacije iz igre !
	public static int SignedIntTobits(int signedint, int signbit) {
		if (signedint < 0) {
			signedint = ~signedint + 1;
			signedint |= signbit;
		} else signedint &= ~signbit;
		return signedint;
	}

	public static byte[] GetBytes(String str) {
		byte[] bytes = new byte[str.Length * sizeof(char)];
		System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
		return bytes;
	}

	public static string GetString(byte[] bytes) {
		char[] chars = new char[bytes.Length / sizeof(char)];
		System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
		return new string(chars);
	}

	public static byte[] AddBytes(ref byte[] b1, ref byte[] b2, bool newbyte) {
		byte[] retval = new byte[b1.Length + b2.Length];
		Buffer.BlockCopy(b1, 0, retval, 0, (newbyte) ? 0 : b1.Length);
		Buffer.BlockCopy(b2, 0, retval, b1.Length, b2.Length);
		return retval;
	}
	public static byte[] AddBytes(ref byte[] b1, ref byte[] b2) {
		return AddBytes(ref b1, ref b2, false);
	}

	// vraca Point sa vrednostima ({1..6}, {1..6}) i suprotnu stranicu od date ('DSIDE side')
	public static Point GetRelativeSideCoord( int x, int y, DSIDE side, ref DSIDE inv_side ) {
		Point ret_pt= new Point();
		switch(side) {
		case DSIDE.LEFT:
			ret_pt.X=x-1;
			ret_pt.Y=y;
			inv_side = DSIDE.RIGHT;
			break;
		case DSIDE.UP:
			ret_pt.X=x;
			ret_pt.Y=y-1;
			inv_side = DSIDE.DOWN;
			break;
		case DSIDE.RIGHT:
			ret_pt.X=x+1;
			ret_pt.Y=y;
			inv_side = DSIDE.LEFT;
			break;
		case DSIDE.DOWN:
			ret_pt.X=x;
			ret_pt.Y=y+1;
			inv_side = DSIDE.UP;
			break;
		}
		return ret_pt;
	}

	public enum DSIDE {
		LEFT = 0,
		UP = 1,
		RIGHT = 2,
		DOWN = 3,
		NONE = 4
	};

	// vraca koja se boja nalazi na datoj strani, nezavisno od zarotiranosti figure
	// bitno je samo da figura stoji horizontalno
	public static int ColorSideHorizontal(Figure fig, DSIDE sd) {
		// skontaj formulu :D
		int col = fig.colors >> ((((3 - (int)sd) + (fig.Angle % 360) / 90) % 4) * 2) & 3;
		// radi kompatibilnosti sa ColorSideVertical (vracaju se 2 iste boje)
		return col | (col << 2);
	}

	// vraca koje 2 boje se nalaze na datoj strani, nezavisno od zarotiranosti figure
	public static int ColorSideVertical(Figure fig, DSIDE sd, bool inverse) {
		// skontaj formule :D
		int angl = (fig.Angle % 360) / 45 - (fig.Angle % 360) / 90;
		int col1, col2;
		// levo 14, gore 12, desno 23, dole 43
		col1 = (fig.colors >> ((((3 - (int)sd) + angl) % 4) * 2) & 3);
		col2 = (fig.colors >> ((((6 - (int)sd) + angl) % 4) * 2) & 3);

		// kompatibilnost sa levom i desnom, gornjom i donjom stranom
		if (inverse)
			return col2 | (col1 << 2);
		else
			return col1 | (col2 << 2);
	}
	public static int ColorSide(Figure fig, DSIDE sd) {
		return fig.Vertical ? ColorSideVertical(fig, sd, false) : ColorSideHorizontal(fig, sd);
	}

	// inverzna varijanta za drugu figuru sa kojom se poredi, koja se onda mora potirati sa neinverznom
	public static int ColorSide(Figure fig, DSIDE sd, bool inverse) {
		return fig.Vertical ? ColorSideVertical(fig, sd, inverse) : ColorSideHorizontal(fig, sd);
	}

	public static DSIDE FindColor(Figure fig, int color) {
		foreach(DSIDE side in Enum.GetValues( typeof(DSIDE) )) {
			if( ColorSide( fig, side ) == color )
				return side;
		}
		return DSIDE.NONE;
	}

	public static bool ColorExist(Figure fig, int color) {
		foreach(DSIDE side in Enum.GetValues( typeof(DSIDE) )) {
			if( ColorSide( fig, side ) == color ) {
				return true;
			}
		}
		return false;
	}

	// vraca pozitivan ugao od odredjene stranice
	public static int SideToAngle(DSIDE side) {
		switch(side) {
		case DSIDE.LEFT:
			return 270;
		case DSIDE.RIGHT:
			return 90;
		case DSIDE.UP:
			return 0;
		case DSIDE.DOWN:
			return 180;
		default:
			return 0;
		}
	}

	public static Point SideToMove(DSIDE side, Point figpos, int figsize) {
		switch(side) {
		case DSIDE.LEFT:
			return new Point( figpos.X - figsize, figpos.Y );
		case DSIDE.RIGHT:
			return new Point( figpos.X + figsize, figpos.Y );
		case DSIDE.UP:
			return new Point( figpos.X, figpos.Y - figsize );
		case DSIDE.DOWN:
			return new Point( figpos.X, figpos.Y + figsize );
		default:
			return figpos;
		}
	}

	// vraca najblizu promenu ugla od odredjene stranice do odredjene stranice
	public static int SideToSideAngleDistance(DSIDE fromSide, DSIDE toSide) {
		int fromAngle = SideToAngle(fromSide);
		int toAngle = SideToAngle(toSide);
		int diff = toAngle - fromAngle;
		if( diff > 180 ) {
			diff = diff - 360;
		} else if( diff < -180 ) {
			diff = diff + 360;
		}
		return diff;
	}

	public static int ClosestAngle(int from_angle, int to_angle) {
		int dif = to_angle - from_angle;

		if(dif > 180) {
			return dif - 360;
		} else if(dif < -180) {
			return dif + 360;
		} else
			return dif;
	}
}
class RandomList {
	Random rnd;
	int[] lista;
	int maxElements;
	int size;
	public RandomList(int alokacija, int seed) {
		lista = new int[alokacija];
		maxElements = alokacija;
		size = 0;
		rnd = new Random(seed);
	}
	public void Push(int broj) {
		if (size < maxElements) {
			int indx;
			indx = rnd.Next(0, size);
			lista[size] = lista[indx];
			lista[indx] = broj;
			size++;
		}
	}

	public int Pop() {
		return lista[--size];
	}
}
}
