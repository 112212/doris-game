﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Doris_Game {
public partial class Singleplayer : Form {
	Pen Black = new Pen(Brushes.Black);
	DrawingDoris gd = new DrawingDoris();

	const int FIGURES_COUNT = 24; // ne menjaj ovu konstantu :)

	FigurePlacer[] g_fpl = new FigurePlacer[FIGURES_COUNT];

	// error detection by Common.DSIDE
	byte[] errors = new byte[FIGURES_COUNT];
	int num_errors = 0;
	int num_attached = 0;

	Figure[] g_fig = new Figure[FIGURES_COUNT];
	FigureContainer[] g_fcon = new FigureContainer[2];

	Point mdiff = new Point(0, 0);
	Point pback= new Point();


	int keyStat = 0;
	int zprio = 0;
	int gameTime = 0;
	const int SIZE = 70; // 80
	Rectangle PlayField = new Rectangle(200, 0, 0, 0);
	Rectangle PlacerBounds = new Rectangle(0, 0, 6, 4); //rezolucija
	bool done=false;

	/* Doris Setup
	 *    O O O O O O
	 *  FigureContainer
	 *    O O O O O O
	 *  +------------+
	 *  |[][][][][][]| <= FigurePlacers
	 *  |[][][][][][]| <= FigurePlacers
	 *  |[][][][][][]| <= FigurePlacers
	 *  +------------+
	 *   O O O O O O
	 *  FigureContainer
	 *   O O O O O O
	 */


	public Singleplayer() {
		InitializeComponent();
	}

	private void Form1_Load(object sender, EventArgs e) {
		gd.SetFigureSize( SIZE );
		for (int i = 0; i < Controls.Count; i++) {
			Controls[i].BackColor = options.textBackColor;
			Controls[i].ForeColor = options.textForeColor;
		}
	}

	public void LoadGame(DorisMap map) {
		this.Show();
		int i;

		PlayField.Size = Size;
		for (i = 0; i < g_fig.Length; i++) {
			g_fig[i] = new Figure(new Point(0, 400), SIZE, i);
			g_fpl[i] = new FigurePlacer(new Point(0, 0), (map.m_pts[i] == 1), SIZE, i);
			g_fig[i].Vertical = false;
			g_fig[i].colors = DrawingDoris.FillCol(DorisGame.FigCols[i]);
		}

		// initialize containers, placers
		PlayField.Width = 6 * SIZE;
		PlayField.Height = 6 * SIZE;
		g_fcon[0] = new FigureContainer(new Point(PlayField.X, PlayField.Y), new Size(6, 2), SIZE, 0);
		g_fcon[1] = new FigureContainer(new Point(PlayField.X, PlayField.Y + SIZE * 6/*-SIZE*2-40*/), new Size(6, 2), SIZE, 1);
		// 2x6 gore, 2x6 dole
		Point psx = new Point();

		for (i = 0; i < FIGURES_COUNT/2; i++)
			g_fcon[0].Attach(ref g_fig[i]);
		for (i = FIGURES_COUNT/2; i < FIGURES_COUNT; i++)
			g_fcon[1].Attach(ref g_fig[i]);

		for (i = 0; i < FIGURES_COUNT; i++)
			errors[i] = 0;

		Point Middle = new Point(PlayField.X, PlayField.Y + SIZE * 2);
		psx.X = 0;
		psx.Y = 0;
		for (i = 0; i < 24; i++) {
			g_fpl[i].m_pos = new Point(Middle.X + psx.X * SIZE, Middle.Y + psx.Y * SIZE);
			if (++psx.X >= 6) {
				psx.Y++;
				psx.X = 0;
			}
		}

		//
		num_attached = 0;
		num_errors = 0;
		gameTime = 0;
		gameTimeTimer.Start();
		loopTimer.Start();
	}

	// Rendering, crtanje
	protected override void OnPaint(PaintEventArgs e) {
		Graphics g = e.Graphics;
		gd.graphics = g;
		Point hwm = new Point(0,0);
		g.Clear(options.backColor);
		g.FillRectangle(options.playFieldColor, PlayField);
		int i;
		for (i = 0; i < 2; i++)
			g_fcon[i].DrawIt(g);
		int len = g_fig.Length;
		for (i = 0; i < len; i++)
			g_fpl[i].DrawIt(gd);

		for (i = 0; i < len; i++)
			if (i != zprio)
				g_fig[i].DrawIt(gd);

		if (keyStat == 0) {
			// selektovanu figuru najzadnju crtamo kako bi bila TOP_MOST.
			g_fig[zprio].DrawIt(gd);
			DrawErrors(g);
		} else {
			DrawErrors(g);
			g_fig[zprio].DrawIt(gd);
		}


		base.OnPaint(e);
	}

	private void TimerLoop_Tick(object sender, EventArgs e) {
		int id;
		switch(Common.LOWORD(keyStat)) {
		case 1:
			id = Common.HIWORD(keyStat);
			if (id < FIGURES_COUNT) {
				Point fpos = this.PointToClient(Cursor.Position);
				fpos.X -= mdiff.X;
				fpos.Y -= mdiff.Y;
				g_fig[id].m_pos = fpos;
			}
			break;
		case 2:
			id = Common.HIWORD(keyStat);
			if (id < FIGURES_COUNT) {
				Point fpos = this.PointToClient(Cursor.Position);
				Point pt = g_fig[id].m_pos;
				g_fig[id].Angle = g_fig[id].fn_Angle(fpos);
				g_fig[id].AngleRound();
				if (Common.HIWORD(g_fig[id].Assignation) == FigurePlacer.ContainerId)
					CheckForErrors(Common.LOWORD(g_fig[id].Assignation));
			}
			break;

		}
		Invalidate();
	}


	private void Form1_MouseDown(object sender, MouseEventArgs e) {
		if(e.Button == MouseButtons.Left)
			if (keyStat == 0 && !done) {
				int len = g_fig.Length;
				Point mpos = this.PointToClient(Cursor.Position);
				for (int i = 0; i < len; i++)
					if (g_fig[i].CheckBounds(mpos)) {
						if (g_fig[i].BoundSquare(mpos)) {
							keyStat |= (i << 8);
							keyStat |= 1;
							mdiff.X = (mpos.X - g_fig[i].m_pos.X);
							mdiff.Y = (mpos.Y - g_fig[i].m_pos.Y);
							pback = g_fig[i].m_pos;
							zprio = i;

							loopTimer.Start();
							return;
						} else if (g_fig[i].CheckPolys(mpos)) {
							keyStat |= (i << 8);
							keyStat |= 2;
							mdiff.X = (mpos.X - g_fig[i].m_pos.X);
							mdiff.Y = (mpos.Y - g_fig[i].m_pos.Y);
							pback = g_fig[i].m_pos;
							zprio = i;
							loopTimer.Start();
						}
					}
			}
	}

	// provera za ubacivanje figure u FigurePlacer
	private bool Check4Assign(int idFig, int idPlacer) {
		if (g_fpl[idPlacer].Assignation >= 0) return false;
		Point fig_pos = g_fig[idFig].m_pos;
		Point plPos = g_fpl[idPlacer].m_pos;
		fig_pos.X += SIZE / 2;
		fig_pos.Y += SIZE / 2;
		if (fig_pos.X > plPos.X &&
		        fig_pos.Y > plPos.Y &&
		        fig_pos.X < plPos.X + SIZE &&
		        fig_pos.Y < plPos.Y + SIZE) {
			DetachAll(idFig);

			g_fpl[idPlacer].Attach(ref g_fig[idFig]);
			OnPlacerAttach(idPlacer);

			return true;
		} else {
			return false;
		}
	}

	// Assignation [ HIDWORD - additional data, HIWORD - ContainerID (which container type), LOWORD( container index ) ]
	private void DetachAll(int i) {
		int asg = g_fig[i].Assignation;
		g_fig[i].Vertical = false; // kad detachujemo vracamo se u horizontalni polozaj
		if (asg > -1) {
			// da li je prikačena na FigurePlacer
			if(Common.HIWORD(asg) == FigurePlacer.ContainerId) {
				OnPlacerDetach(Common.LOWORD(asg));
				g_fpl[ Common.LOWORD(asg) ].Detach();
			}
			// da li je prikačena na FigureContainer
			else if (Common.HIWORD(asg) == FigureContainer.ContainerId) {
				for (int a = 0; a < 2; a++) {
					if(g_fcon[a].Detach(ref g_fig[i])) return;
				}
			}
		}
	}

	// provera jedne figure sa susednim da li se boje poklapaju
	private bool CheckCollision(int i) {
		bool retval = true;
		int id;
		int id2 = g_fpl[i].Assignation;

		if (id2 == -1) return false;

		// leva strana
		if (i - 1 >= 0 && (i-1) % PlacerBounds.Width != PlacerBounds.Width-1) {
			id = g_fpl[i - 1].Assignation;
			if (id != -1) {
				if (Common.ColorSide(g_fig[id2], Common.DSIDE.LEFT) != Common.ColorSide(g_fig[id], Common.DSIDE.RIGHT, true)) {
					SetFigurePlacerError(i, Common.DSIDE.LEFT, true);
					retval = false;
				} else
					SetFigurePlacerError(i, Common.DSIDE.LEFT, false);
			}
		}
		// desna strana
		if (i + 1 < FIGURES_COUNT && (i + 1) % PlacerBounds.Width != 0) {
			id = g_fpl[i + 1].Assignation;
			if (id != -1) {
				if (Common.ColorSide(g_fig[id2], Common.DSIDE.RIGHT) != Common.ColorSide(g_fig[id], Common.DSIDE.LEFT, true)) {
					SetFigurePlacerError(i, Common.DSIDE.RIGHT, true);
					retval = false;
				} else
					SetFigurePlacerError(i, Common.DSIDE.RIGHT, false);
			}
		}
		// gornja strana
		if (i - PlacerBounds.Width >= 0) {
			id = g_fpl[i - PlacerBounds.Width].Assignation;
			if (id != -1) {
				if (Common.ColorSide(g_fig[id2], Common.DSIDE.UP) != Common.ColorSide(g_fig[id], Common.DSIDE.DOWN, true)) {
					SetFigurePlacerError(i, Common.DSIDE.UP, true);
					retval = false;
				} else
					SetFigurePlacerError(i, Common.DSIDE.UP, false);
			}
		}
		// donja strana
		if (i + PlacerBounds.Width < FIGURES_COUNT) {
			id = g_fpl[i + PlacerBounds.Width].Assignation;
			if (id != -1) {
				if (Common.ColorSide(g_fig[id2], Common.DSIDE.DOWN) != Common.ColorSide(g_fig[id], Common.DSIDE.UP, true)) {
					SetFigurePlacerError(i, Common.DSIDE.DOWN, true);
					retval = false;
				} else
					SetFigurePlacerError(i, Common.DSIDE.DOWN, false);
			}
		}
		return retval;
	}


	private void Form1_MouseUp(object sender, MouseEventArgs e) {
		loopTimer.Stop();
		bool chk = false;
		if(Common.LOWORD(keyStat)==1)

			// provera za attach na neko polje u playgroundu
			for (int i = 0; i < FIGURES_COUNT; i++) {
				if (Check4Assign(zprio,i)) {
					chk = true;
					break;
				}
			}

		Point pos = g_fig[zprio].m_pos;

		pos.X += SIZE/2;
		pos.Y += SIZE/2;

		if (!chk && Common.LOWORD(keyStat)==1) {
			// provera za attach na jedan od 2 kontejnera
			for (int a = 0; a < 2; a++) {
				// provera da li drzimo mis na kontaineru
				if (g_fcon[a].CheckBounds(pos)) {

					if(g_fig[zprio].Assignation >> 16 == a &&
					        (Common.HIWORD(g_fig[zprio].Assignation)) == FigureContainer.ContainerId) {
						chk = false;
						break;
					}

					// detach iz playgrounda
					DetachAll(zprio);

					// attach nazad u container
					if (g_fcon[a].Attach(ref g_fig[zprio])) {
						chk = true;
						break;
					}
				}
			}
		}


		if (!chk && Common.LOWORD(keyStat) == 1) {
			g_fig[zprio].m_pos = pback;
		}

		// provera da li da proveravamo podudaranje boja xD
		if (num_attached == FIGURES_COUNT) {
			if (num_errors == 0) {
				gameTimeTimer.Stop();
				Invalidate();
				MessageBox.Show("Igra završena za: " + Common.FormatTime(gameTime));
			}
		}
		keyStat = 0;
		Invalidate();
	}

	// poziva se posle attach figure na placera
	private void OnPlacerAttach( int placerId ) {
		num_attached++;
		CheckCollision(placerId);
	}

	private void CheckForErrors(int placerId) {
		CheckCollision(placerId);
	}

	// poziva se pre detach figure sa placera
	private void OnPlacerDetach( int placerId ) {
		num_attached--;
		if (errors[placerId] == 0) return;

		for(int i=0; i < 4; i++)
			if ((errors[placerId] & (1 << i)) != 0) {
				SetFigurePlacerError(placerId, (Common.DSIDE)i, false);
			}
	}

	private void SetFigurePlacerError(int figurePlacerId, Common.DSIDE side, bool value) {
		int id2=-1;
		byte sideBit = (byte)(1 << (byte)side);
		if (((errors[figurePlacerId] & sideBit) != 0) == value)
			return;

		if (value) {
			errors[figurePlacerId] |= sideBit;
			num_errors++;
		} else {
			errors[figurePlacerId] &= (byte)(~sideBit);
			num_errors--;
		}


		switch(side) {
		case Common.DSIDE.LEFT:
			if (figurePlacerId / PlacerBounds.Width == (figurePlacerId - 1) / PlacerBounds.Width)
				id2 = figurePlacerId - 1;
			break;
		case Common.DSIDE.RIGHT:
			if (figurePlacerId / PlacerBounds.Width == (figurePlacerId + 1) / PlacerBounds.Width)
				id2 = figurePlacerId + 1;
			break;
		case Common.DSIDE.UP:
			id2 = figurePlacerId - PlacerBounds.Width;
			break;
		case Common.DSIDE.DOWN:
			id2 = figurePlacerId + PlacerBounds.Width;
			break;
		}

		// idemo suprotnim stranama
		switch (side) {
		case Common.DSIDE.LEFT:
			side = Common.DSIDE.RIGHT;
			break;
		case Common.DSIDE.RIGHT:
			side = Common.DSIDE.LEFT;
			break;
		case Common.DSIDE.UP:
			side = Common.DSIDE.DOWN;
			break;
		case Common.DSIDE.DOWN:
			side = Common.DSIDE.UP;
			break;
		}
		sideBit = (byte)(1 << (byte)side);

		// da li je id2 u granicama PlacerBounds
		if (id2 >= 0 && id2 < PlacerBounds.Width * PlacerBounds.Height) {
			if (value)
				errors[id2] |= sideBit;
			else
				errors[id2] &= (byte)(~sideBit);
		} else {
			//~ MessageBox.Show("SetFigureError() exception");
		}
	}

	private void DrawErrors(Graphics g) {
		if (!options.errorChecking || num_errors == 0) return;
		for(int i=0; i < FIGURES_COUNT; i++) {
			if (g_fpl[i].Assignation >= 0 && errors[i] > 0) {
				gd.DrawOutlinePolygons(g, g_fpl[i].m_pos, errors[i]);
			}
		}
	}

	private void GameTimeTimer_Tick(object sender, EventArgs e) {
		gameTime++;
		label1.Text = Common.FormatTime(gameTime);
	}

	private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
		this.Hide();
		Program.SForm.Show();
		e.Cancel = true;
	}
}
}
