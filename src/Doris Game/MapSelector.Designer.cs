﻿namespace Doris_Game {
partial class MapSelector {
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Windows Form Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
		this.FlowPanel = new System.Windows.Forms.FlowLayoutPanel();
		this.but_ok = new System.Windows.Forms.Button();
		this.but_del = new System.Windows.Forms.Button();
		this.but_new = new System.Windows.Forms.Button();
		this.SuspendLayout();
		//
		// FlowPanel
		//
		this.FlowPanel.AutoScroll = true;
		this.FlowPanel.Location = new System.Drawing.Point(2, 12);
		this.FlowPanel.Name = "FlowPanel";
		this.FlowPanel.Size = new System.Drawing.Size(644, 473);
		this.FlowPanel.TabIndex = 0;
		//
		// but_ok
		//
		this.but_ok.Location = new System.Drawing.Point(668, 437);
		this.but_ok.Name = "but_ok";
		this.but_ok.Size = new System.Drawing.Size(92, 45);
		this.but_ok.TabIndex = 1;
		this.but_ok.Text = "OK";
		this.but_ok.UseVisualStyleBackColor = true;
		this.but_ok.Click += new System.EventHandler(this.but_ok_Click);
		//
		// but_del
		//
		this.but_del.Location = new System.Drawing.Point(668, 344);
		this.but_del.Name = "but_del";
		this.but_del.Size = new System.Drawing.Size(92, 40);
		this.but_del.TabIndex = 2;
		this.but_del.Text = "Delete";
		this.but_del.UseVisualStyleBackColor = true;
		this.but_del.Click += new System.EventHandler(this.btn_del_Click);
		//
		// but_new
		//
		this.but_new.Location = new System.Drawing.Point(668, 390);
		this.but_new.Name = "but_new";
		this.but_new.Size = new System.Drawing.Size(92, 41);
		this.but_new.TabIndex = 3;
		this.but_new.Text = "New Map";
		this.but_new.UseVisualStyleBackColor = true;
		this.but_new.Click += new System.EventHandler(this.btn_new_Click);
		//
		// MapSelector
		//
		this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
		this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		this.ClientSize = new System.Drawing.Size(785, 499);
		this.Controls.Add(this.but_new);
		this.Controls.Add(this.but_del);
		this.Controls.Add(this.but_ok);
		this.Controls.Add(this.FlowPanel);
		this.Name = "MapSelector";
		this.Text = "MapSelector";
		this.Load += new System.EventHandler(this.MapSelector_Load);
		this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MapSelector_FormClosing);
		this.ResumeLayout(false);

	}

	#endregion

	private System.Windows.Forms.FlowLayoutPanel FlowPanel;
	private System.Windows.Forms.Button but_ok;
	private System.Windows.Forms.Button but_del;
	private System.Windows.Forms.Button but_new;
}
}