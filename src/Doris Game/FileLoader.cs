﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
namespace Doris_Game {

class Ft_ {
	int ls=0;
	int code=0;
	String Buff;
	String strToFt;
	bool cryption = false;
	bool eof = false;
	const int LenLoad = 0x1;
	const int ContLoad = 0x2;
	const int enc=0x1;
	const int dec=0x2;
	int cryptmet = 5;
	void m_cryption(ref String str, int w) {
		int len = str.Length;
		bool ch = (w==enc)?true:false;
		String oth = "";
		for (int i = 0; i < len; i++,ch^=true) {
			if (ch)
				oth += (char)(str[i]+cryptmet);
			else
				oth += (char)(str[i]-cryptmet);
		}
		str = oth;
	}
	public void CreateFT(String str,bool encryption) {
		Cleanup();
		Buff = str;
		cryption = encryption;
	}
	public void CreateFT(String str) {
		Cleanup();
		Buff = str;
	}
	public void CreateFT() {
		Cleanup();
		Buff = "";
	}
	public void NewFt(String str) {
		Cleanup();
		strToFt = str;
	}
	public void NewFt(String str, bool encryption) {
		Cleanup();
		strToFt = str;
		cryption = encryption;
	}
	public int Code {
		get {
			return code;
		}
	}
	public String Text {
		get {
			return Buff;
		}
	}
	public int CryptCode {
		get {
			return cryptmet;
		} set {
			cryptmet = value;
		}
	}
	public bool Cryption {
		get {
			return cryption;
		} set {
			cryption = value;
		}
	}
	public bool EndOfFetch {
		get {
			return eof;
		} set {
			eof = value;
		}
	}
	public void Fetch() {
		Buff = "";
		int len = strToFt.Length;
		int curSq = LenLoad;
		int cnt = 0;
		int lenLoaded = 0;
		int i = 0;
		for (i = ls; i < len; i++) {
			switch (curSq) {
			case LenLoad:
				code = strToFt[i];
				lenLoaded = ((strToFt[i+1]-20) << 8) |
				            (strToFt[i + 2]-20);
				i += 2;
				curSq = ContLoad;
				break;
			case ContLoad:
				if (cnt++ < lenLoaded)
					Buff += strToFt[i];
				else {
					ls = i;
					if (cryption)m_cryption(ref Buff, dec);
					return;
				}
				break;
			}
		}
		ls = i;
		if (cryption) m_cryption(ref Buff, dec);
		if (i >= len) eof = true;
	}
	public void Cleanup() {
		Buff = "";
		strToFt = "";
		code = 0;
		eof = false;
		ls = 0;
	}
	public void MakeFt(String toAdd, int code) {
		int len = toAdd.Length;
		Buff += (char)(code&0xff);
		Buff += (char)(((len&0xff00)>>8)+20);
		Buff += (char)((len&0xff)+20);
		if (cryption)
			m_cryption(ref toAdd, enc);
		Buff += toAdd;
	}
}


class FileLoader {
	const int MAP_TYPE = 70;
	const int MAP_RESOLUTION_X = 55;
	const int MAP_RESOLUTION_Y = 56;
	const int MAP_BYTES = 75;
	const int MAP_TITLE = 74;
	const int MAP = 159;
	public void LoadFile(ref DorisMap[] dg, String file, bool Crypt) {
		StreamReader fre = new StreamReader(file);
		String buff=fre.ReadToEnd();
		int cnt = 0;
		if (buff.Length == 0) return;
		int len = buff[0];
		dg = new DorisMap[len];
		Ft_ Ft = new Ft_();
		Ft_ Ftc = new Ft_();
		for (Ft.NewFt(buff.Substring(1), Crypt); !Ft.EndOfFetch; cnt++) {
			Ft.Fetch();
			if (Ft.Code == MAP) {
				dg[cnt].id = cnt;
				for (Ftc.NewFt(Ft.Text, Crypt); !Ftc.EndOfFetch; ) {
					Ftc.Fetch();
					switch (Ftc.Code) {
					case MAP_TYPE:
						dg[cnt].m_map_type = (MapType)Ftc.Text[0];
						break;
					case MAP_RESOLUTION_X:
						dg[cnt].m_resolution.X = (int)Ftc.Text[0];
						break;
					case MAP_RESOLUTION_Y:
						dg[cnt].m_resolution.Y = (int)Ftc.Text[0];
						break;
					case MAP_BYTES:
						if (dg[cnt].m_map_type == MapType.Challenge)
							dg[cnt].m_pts = WCBToMapStruct(Ftc.Text.ToCharArray());
						else
							dg[cnt].fig_placers = DeserializeFigurePlacers(Ftc.Text.ToCharArray());
						break;
					case MAP_TITLE:
						dg[cnt].Title = Ftc.Text;
						break;
					}
				}
			}
			Ftc.Cleanup();
		}
		fre.Close();
	}

	// WorkChallengeBytes
	private byte[] WCBToMapStruct(char[] bytes) {
		int len = bytes.Length*8;
		byte[] bytesMap = new byte[len];
		for (int i = 0; i < len; i++) {
			bytesMap[i] = (byte)((bytes[i / 8] >> (i % 8)) & 0x1);
		}
		return bytesMap;
	}
	private char[] WCBToMapFile(byte[] bytesMap) {
		int len = bytesMap.Length;
		char[] retval = new char[len/8];
		int len2 = retval.Length;
		for (int i = 0; i < len; i++) {
			retval[i / 8] |= (char)(bytesMap[i] << (i % 8));
		}
		return retval;
	}

	private char[] SerializeFigurePlacers(FigurePlacer[] placers) {
		char[] retval = new char[ placers.Length * 3 ];
		int j;
		for(int i=0; i < placers.Length; i++ ) {
			j = 3*i;
			retval[ j ] = (char)placers[i].m_loc.X;
			retval[ j + 1] = (char)placers[i].m_loc.Y;
			retval[ j + 2 ] = placers[i].Vertical ? '1' : '0' ;
		}
		return retval;
	}
	private FigurePlacer[] DeserializeFigurePlacers(char[] bytes) {
		int figures = bytes.Length / 3;
		int j;
		FigurePlacer[] placers = new FigurePlacer[figures];
		for (int i = 0; i < figures; i++) {
			j = 3 * i;
			placers[i].m_loc.X = bytes[j];
			placers[i].m_loc.Y = bytes[j + 1];
			placers[i].Vertical = bytes[j + 2] == '1';
		}
		return placers;
	}

	public void WriteToFile(DorisMap[] dg, String file, bool Crypt) {
		StreamWriter fwr = new StreamWriter(file);
		Ft_ Ft = new Ft_();
		Ft_ Ftc = new Ft_();
		int len = dg.Length;
		Ft.Cryption = Crypt;
		Ftc.Cryption = Crypt;
		fwr.Write(((char)dg.Length));
		Ft.CreateFT();
		for (int i = 0; i < len; i++) {
			switch (dg[i].m_map_type) {
			case MapType.Challenge: {
				Ftc.CreateFT();
				Ftc.MakeFt(dg[i].Title, MAP_TITLE);
				Ftc.MakeFt(((char)dg[i].m_map_type).ToString(), MAP_TYPE);
				Ftc.MakeFt(((char)dg[i].m_resolution.X).ToString(), MAP_RESOLUTION_X);
				Ftc.MakeFt(((char)dg[i].m_resolution.Y).ToString(), MAP_RESOLUTION_Y);
				Ftc.MakeFt(new String(WCBToMapFile(dg[i].m_pts)), MAP_BYTES);
			}
			break;
			case MapType.Puzzle: {
				Ftc.CreateFT();
				Ftc.MakeFt(dg[i].Title, MAP_TITLE);
				Ftc.MakeFt(((char)dg[i].m_map_type).ToString(), MAP_TYPE);
				Ftc.MakeFt(((char)dg[i].m_resolution.X).ToString(), MAP_RESOLUTION_X);
				Ftc.MakeFt(((char)dg[i].m_resolution.Y).ToString(), MAP_RESOLUTION_Y);
				Ftc.MakeFt(new String(SerializeFigurePlacers(dg[i].fig_placers)), MAP_BYTES);
			}
			break;
			}
			Ft.MakeFt(Ftc.Text, MAP);
		}
		fwr.Write(Ft.Text);
		Ft.Cleanup();
		fwr.Close();
	}
}
}
