﻿namespace Doris_Game {
partial class MapMaker {
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Windows Form Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
		this.tb_game = new System.Windows.Forms.TextBox();
		this.label1 = new System.Windows.Forms.Label();
		this.label2 = new System.Windows.Forms.Label();
		this.tb_res_A = new System.Windows.Forms.TextBox();
		this.but_save = new System.Windows.Forms.Button();
		this.label3 = new System.Windows.Forms.Label();
		this.SuspendLayout();
		//
		// tb_game
		//
		this.tb_game.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
		this.tb_game.Location = new System.Drawing.Point(302, 27);
		this.tb_game.Name = "tb_game";
		this.tb_game.Size = new System.Drawing.Size(369, 31);
		this.tb_game.TabIndex = 0;
		//
		// label1
		//
		this.label1.AutoSize = true;
		this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
		this.label1.Location = new System.Drawing.Point(197, 34);
		this.label1.Name = "label1";
		this.label1.Size = new System.Drawing.Size(99, 24);
		this.label1.TabIndex = 1;
		this.label1.Text = "ime mape:";
		//
		// label2
		//
		this.label2.AutoSize = true;
		this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
		this.label2.Location = new System.Drawing.Point(197, 72);
		this.label2.Name = "label2";
		this.label2.Size = new System.Drawing.Size(126, 24);
		this.label2.TabIndex = 3;
		this.label2.Text = "koliko redova:";
		//
		// tb_res_A
		//
		this.tb_res_A.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
		this.tb_res_A.Location = new System.Drawing.Point(332, 68);
		this.tb_res_A.Name = "tb_res_A";
		this.tb_res_A.Size = new System.Drawing.Size(104, 31);
		this.tb_res_A.TabIndex = 2;
		this.tb_res_A.TextChanged += new System.EventHandler(this.tb_res_A_TextChanged);
		//
		// but_save
		//
		this.but_save.Location = new System.Drawing.Point(660, 563);
		this.but_save.Name = "but_save";
		this.but_save.Size = new System.Drawing.Size(175, 39);
		this.but_save.TabIndex = 7;
		this.but_save.Text = "Sacuvaj";
		this.but_save.UseVisualStyleBackColor = true;
		this.but_save.Click += new System.EventHandler(this.but_save_Click);
		//
		// label3
		//
		this.label3.AutoSize = true;
		this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
		this.label3.Location = new System.Drawing.Point(160, 546);
		this.label3.Name = "label3";
		this.label3.Size = new System.Drawing.Size(78, 24);
		this.label3.TabIndex = 8;
		this.label3.Text = "Prevuci:";
		//
		// MapMaker
		//
		this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
		this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		this.ClientSize = new System.Drawing.Size(959, 635);
		this.Controls.Add(this.label3);
		this.Controls.Add(this.but_save);
		this.Controls.Add(this.label2);
		this.Controls.Add(this.tb_res_A);
		this.Controls.Add(this.label1);
		this.Controls.Add(this.tb_game);
		this.DoubleBuffered = true;
		this.Name = "MapMaker";
		this.Text = "MapMaker";
		this.Load += new System.EventHandler(this.MapMaker_Load);
		this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MapMaker_MouseUp);
		this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MapMaker_MouseDown);
		this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MapMaker_FormClosing);
		this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MapMaker_MouseMove);
		this.ResumeLayout(false);
		this.PerformLayout();

	}

	#endregion

	private System.Windows.Forms.TextBox tb_game;
	private System.Windows.Forms.Label label1;
	private System.Windows.Forms.Label label2;
	private System.Windows.Forms.TextBox tb_res_A;
	private System.Windows.Forms.Button but_save;
	private System.Windows.Forms.Label label3;

}
}